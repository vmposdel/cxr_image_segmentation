import argparse
import csv
import itertools

from src.main import run_experiment
from src.config import Params

def test_params():
    param_vals = {
            "convex_hull_unconstrained_fill_max_distance_start_to_end": [0],
            "convex_hull_unconstrained_fill_max_distance_to_farthest": [0],
            "convex_hull_mp_points_fill_max_distance_start_to_end": [150],
            "convex_hull_mp_points_fill_max_distance_to_farthest": [150],
            "convex_hull_mp_points_fill_min_density": [0.0001, 0.001, 0.005, 0.01, 0.02, 0.05, 0.1, 0.25],
        }
    param_names = list(param_vals.keys())
    for params in itertools.product(*param_vals.values()):
      current_param_vals = {}
      for param_name, param_val in zip(param_names, params):
        current_param_vals[param_name] = param_val
      yield Params(**current_param_vals)

      
def get_param_names():
    return list(Params()._asdict().keys())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--raw", help="Convert raw images to png, save them and then load them from the files", action="store_true")
    parser.add_argument("--simple", help="Use simple segmentator", action="store_true")
    parser.add_argument("--wavelet", help="Use wavelet transform to segment", action="store_true")
    parser.add_argument("--hough", help="Use hough segmentator", action="store_true")
    parser.add_argument("--haar_growable_hough", help="Use haar growable hough segmentator", action="store_true")
    parser.add_argument("--border_growable_hough", help="Use border growable hough segmentator", action="store_true")
    parser.add_argument("--jsrt", help="Use JSRT Dataset", action="store_true")
    parser.add_argument("--montgomery", help="Use Montgomery Dataset", action="store_true")
    parser.add_argument("--write-to-file", help="Write scores to file", action="store_true")
    parser.add_argument("--compare-with-file", help="Compare with scores in file")
    parser.add_argument("--experiment-name", help="Experiment name under which to write output")
    parser.add_argument("--adaptive-thresholding", help="Use most probable lung point to select threshold per image",
                        action="store_true")
    args = parser.parse_args()

    max_dices = 0.
    best_params = None
    csv_columns = get_param_names() + ["Jaccard", "Dices"]
    with open('res8-04-2020.csv', 'w') as f:
        writer = csv.DictWriter(f, fieldnames=csv_columns)
        writer.writeheader()
        for params in test_params():
            output = params._asdict()
            jaccard, dices = run_experiment(args, params)
            output['Jaccard'] = jaccard
            output['Dices'] = dices
            writer.writerow(output)

            if dices > max_dices:
                max_dices = dices
                best_params = params
                print(f"New max dices: {max_dices} for params {params}")
    print(f"Best Params: {best_params}")

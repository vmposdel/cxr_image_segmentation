import cv2 as cv
import numpy as np

class MorphologicalTransformer:
  def __init__(self, erosion_kernel_size, erosion_iterations, dilation_kernel_size, dilation_iterations):
    self._erosion_kernel = np.ones((erosion_kernel_size, erosion_kernel_size), np.uint8)
    self._erosion_iterations = erosion_iterations
    self._dilation_kernel = np.ones((dilation_kernel_size, dilation_kernel_size), np.uint8)
    self._dilation_iterations = dilation_iterations
 
  def erode(self, image):
    return cv.erode(image, self._erosion_kernel, iterations = self._erosion_iterations)

  def dilate(self, image):
    return cv.dilate(image, self._dilation_kernel, iterations = self._dilation_iterations)


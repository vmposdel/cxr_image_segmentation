import cv2 as cv
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from csv import reader, excel_tab
from os import listdir
import tensorflow as tf
import copy
import math
import re
import os

from src.segmentation_evaluator import EvaluationImage
from src.contour_extractor import ContourExtractor

class MontgomeryImage():
    def __init__(self):
        self.image = None
        self.image_path = None
        self.image_height = None
        self.image_width = None
        self._image_type = None
        self._diagnosis = None
        self._age = None
        self._sex = None

    def opencv_load_from_file(self, path):
        """ Load image with OpenCV. """
        self.image_path = re.sub('\.png$', '', path)
        orig_image = cv.imread(path, cv.CV_8UC1)
        self.image = cv.resize(orig_image, (640, 525))
        dimensions = self.image.shape
        self.image_height = dimensions[0]
        self.image_width = dimensions[1]
        return self

    def add_description(self, data):
        """ This function adds additional details to the MontgomeryImage object.

        Args:
            data       (list): data is the content of a clinical info file containing is a specific
            format these info: age, sex, diagnosis

        """
        for row in data:
          if 'Sex' in row:
              self._sex = row.replace("Patient's Sex: ","")
          elif 'Age' in row:
              self._age = row.replace("Patient's Age: ","").replace()
          else:
              self._diagnosis = row

        return self

class MontgomeryGTImage(EvaluationImage):
    def __init__(self):
        super().__init__()

    def opencv_load_from_file(self, path, right_part):
        """ Load image with OpenCV. """
        orig_image = cv.imread(path, cv.CV_8UC1)
        if right_part:
            self._right_part_path = re.sub('\.png$', '', path)
            self._right_part_image = cv.resize(orig_image, (640, 525))
        else:
            self._left_part_path = re.sub('\.png$', '', path)
            self._left_part_image = cv.resize(orig_image, (640, 525))
        dimensions = self._right_part_image.shape
        self.image_height = dimensions[0]
        self.image_width = dimensions[1]
        return self

    def calculate_contours(self):
        self.left_part_contours, contour_img_left = ContourExtractor.get_for_image(self._left_part_image)
        self.right_part_contours, contour_img_right = ContourExtractor.get_for_image(self._right_part_image)
        return


class Montgomery:

    def __init__(self):
        self._images_dir = None
        self._clinical_info_dir = None
        self._image_list = None
        self._gt_images = None
        self._title_gt_image_mapping = None 

        self.test_dataset = None
        self.valid_dataset = None
        self.train_dataset = None


    def load_images(self, images_path, clinical_info_path, num_of_images=None):
        self._images_dir = images_path
        self._clinical_info_dir = clinical_info_path
        self._get_images_list(num_of_images)
        self.add_descriptions_to_image()
        return self

    def _get_images_list(self, num_of_images=None):
        images_list = [f for f in listdir(self._images_dir) if not f.startswith('.') and f.endswith(".png")]
        if num_of_images is not None:
          images_list = images_list[:num_of_images]
        files = []
        # Non-nodule and has-nodule filename are separated based on filename.
        for filename in images_list:
            files.append(filename)

        self._image_list = self._opencv_load_images_from_png(files, self._images_dir)
        return self

    def load_gt_images(self, gt_images_path):
        filenames = []
        for image in self._image_list:
            filenames.append(os.path.basename(image.image_path) + ".png")
        right_part_dir = gt_images_path + "rightMask/"
        left_part_dir = gt_images_path + "leftMask/"
        self._gt_images, self._title_gt_image_mapping = self._opencv_load_gt_images_from_png(filenames, right_part_dir, left_part_dir)
        return self

    def add_descriptions_to_image(self):
        """ This function fetches the descriptions of the images present in the csv file and adds them
            to the Image object.
        """
        for image in self._image_list:
            with open(self._clinical_info_dir + os.path.basename(image.image_path) + ".txt") as data:
                content = reader(data)
                image.add_description(content)

    def get_images(self, num_of_images=1):
        """ This function gives "num_of_images" number of MontgomeryImage objects in a list.

        Args:
            num_of_images (int): Defaults to 1. The required number of images is given.

        Returns:
            a list of MontgomeryImage objects. Total objects will be num_of_images.

        """
        if len(self._image_list) < num_of_images:
            print("Number of images available is " + str(len(self._image_list)))
            return -1
        return self._image_list[:num_of_images]

    def get_gt_images(self, num_of_images=1):
        """ This function gives "num_of_images" number of MontgomeryGTImage objects in a list.

        Args:
            num_of_images (int): Defaults to 1. The required number of images is given.

        Returns:
            a list of MontgomeryGTImage objects. Total objects will be num_of_images.

        """
        if len(self._gt_images) < num_of_images:
            print("Number of images available is " + str(len(self._gt_images)))
            return -1
        return self._gt_images[:num_of_images]

    def get_title_gt_image_mapping(self):
        return self._title_gt_image_mapping

    @staticmethod
    def _opencv_load_gt_images_from_png(filenames, right_part_dir, left_part_dir):
        """ This function load images (not image) located at directory/filename and creates an image object from it.
        Args:
            filenames (list): a list of names of the  image files.
            right_part_dir (str): path to the directory/folder where all right part images are present
            left_part_dir (str): path to the directory/folder where all left part images are present
        Returns:
            a list of MontgomeryGTImage objects, with the appropriate OpenCV images
            a mapping of the image title to the index of the image  in aforementioned list

        """
        images_list = []
        title_gt_image_mapping = {}
        for filename in filenames:
            img = MontgomeryGTImage()
            img.opencv_load_from_file(right_part_dir + filename, right_part = True)
            img.opencv_load_from_file(left_part_dir + filename, right_part = False)
            img.calculate_contours()
            images_list.append(img)
            title_gt_image_mapping[filename.replace(".png", "")] = len(images_list) - 1
        return images_list, title_gt_image_mapping

    @staticmethod
    def _opencv_load_images_from_png(filenames, directory):
        """ This function load images (not image) located at directory/filename and creates an image object from it.
        Args:
            filenames (list): a list of names of the image files.
            directory  (str): path to the directory/folder where all images are present
        Returns:
            a list of MontgomeryImage objects, with the appropriate OpenCV image as image

        """
        images_list = []
        for image_name in filenames:
            img = MontgomeryImage()
            img.opencv_load_from_file(directory + image_name)
            images_list.append(img)
        return images_list

import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np
from src.config import VISUALIZATION

class ContourExtractor():
  def __init__(self, segmented_images = []):
    self._segmented_images = segmented_images

  @staticmethod
  def get_for_image(image, image_title = ""):
    _, contours, _ =cv.findContours(image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    im2 =  np.zeros(image.shape, np.uint8)
    cv.drawContours(im2, contours, -1, (255, 0, 0), 3)
    return contours, im2
    
  def get(self):
    for image in self._segmented_images:
      yield ContourExtractor.get_for_image(image)

  @staticmethod
  def contours_filter(im_shape, contours, condition):
    contours_to_keep = []
    for contour in contours:
      if condition(contour):
        if contour not in contours_to_keep:
            contours_to_keep.append(contour)
    contours_to_keep = np.array(contours_to_keep)
    im =  np.zeros(im_shape, np.uint8)
    cv.drawContours(im, contours_to_keep, -1, (255, 0, 0), 3)
    return contours_to_keep, im
    

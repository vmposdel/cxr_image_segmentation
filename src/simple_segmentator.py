import cv2 as cv
from matplotlib import pyplot as plt
from src.base_segmentator import BaseSegmentator
from src.contour_extractor import ContourExtractor
from src.morphological_transformer import MorphologicalTransformer

class SimpleSegmentator(BaseSegmentator):
  def __init__(self, images, image_paths, params, morphological_transformer = None):
    self._images = images
    self._image_paths = image_paths
    self._params = params
    self._morphological_transformer = morphological_transformer if morphological_transformer is not None \
        else MorphologicalTransformer(params.erosion_kernel_size, params.erosion_iterations,
                                      params.dilation_kernel_size, params.dilation_iterations)

  def segment(self):
    for image, image_title in zip(self._images, self._image_paths):
      yield self.segment_by_thresholding(image, image_title)

  def segment_one(self, image, image_title, adaptive=False, otsu=False,
                  intensity_lower_threshold=None, intensity_upper_threshold=None):
    if adaptive:
        return self.segment_by_adaptive_thresholding(image, image_title)
    elif otsu:
        return self.segment_by_otsu_thresholding(image, image_title)
    else:
        return self.segment_by_thresholding(image, image_title,
                                            intensity_lower_threshold if intensity_lower_threshold is not None \
                                                else self._params.intensity_lower_threshold,
                                            intensity_upper_threshold if intensity_upper_threshold is not None \
                                                else self._params.intensity_upper_threshold)

  def segment_by_thresholding(self, image, image_title, intensity_lower_threshold,
                              intensity_upper_threshold):
    ret, mask_non_zero = cv.threshold(image, intensity_lower_threshold, 255, cv.THRESH_BINARY)
    ret, thresh = cv.threshold(image, intensity_upper_threshold, 255, cv.THRESH_BINARY_INV)
    thresh_masked = cv.bitwise_and(thresh, thresh, mask = mask_non_zero)
    if self._morphological_transformer is not None:
      thresh_masked = self._morphological_transformer.erode(thresh_masked)
    contours, contour_img = ContourExtractor.get_for_image(thresh_masked)
    return thresh_masked, contours, contour_img

  def segment_by_adaptive_thresholding(self, image, image_title):
    thresh = cv.adaptiveThreshold(image, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 11, 2)
    if self._morphological_transformer is not None:
      thresh = self._morphological_transformer.erode(thresh)
    contours, contour_img = ContourExtractor.get_for_image(thresh)
    return thresh, contours, contour_img

  def segment_by_otsu_thresholding(self, image, image_title):
    ret, mask_non_zero = cv.threshold(image, self._params.intensity_lower_threshold, 255, cv.THRESH_BINARY)
    ret, thresh = cv.threshold(image, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)
    thresh_masked = cv.bitwise_and(thresh, thresh, mask = mask_non_zero)
    if self._morphological_transformer is not None:
      thresh_masked = self._morphological_transformer.erode(thresh_masked)
    contours, contour_img = ContourExtractor.get_for_image(thresh_masked)
    return thresh_masked, contours, contour_img

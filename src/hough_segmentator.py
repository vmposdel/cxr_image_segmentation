from collections import defaultdict
import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np
from typing import NamedTuple, Sequence, Tuple

from src.base_segmentator import BaseSegmentator
from src.contour_extractor import ContourExtractor
from src.morphological_transformer import MorphologicalTransformer
from src.simple_segmentator import SimpleSegmentator
from src.visualization_utils import EvolvingImage
from src.util import reject_outliers


def get_pixel_intensity_at_axis_graph(image, points, params, axis = 0):
    coord_intensity_map = defaultdict(list)
    im_height, im_width = image.shape
    for mp_x, mp_y in points:
      bb_img = image[max(int(mp_y - (params.histogram_bb_size / 2)), 0) : min(int(mp_y + (params.histogram_bb_size / 2)), im_height),
                     max(int(mp_x - (params.histogram_bb_size / 2)), 0) : min(int(mp_x + (params.histogram_bb_size / 2)), im_width)]
      val = bb_img.mean()
      if axis == 0:
        coord_intensity_map[mp_y].append(val)
      else:
        coord_intensity_map[mp_x].append(val)
    
    coords = sorted(list(coord_intensity_map.keys()))
    # bins = np.linspace(coords[0], coords[-1], 20)
    bins = np.linspace(0, im_height if axis == 0 else im_width, 25)
    bin_intensity_map = defaultdict(list)
    bins_occupied = []

    for bi in range(0, len(bins) - 1):
      for coord in coords:
        if bins[bi] <= coord <= bins[bi + 1]:
          bin_intensity_map[bins[bi]] += coord_intensity_map[coord]
      if bins[bi] in bin_intensity_map:
          bins_occupied.append((bins[bi] + bins[bi + 1]) / 2)


    intensities = [sum(vals) / len(vals) for _, vals in bin_intensity_map.items()]
    return bins_occupied, intensities
        

def get_hist_for_points(image, points, params, draw = None, min_intensity = 0.):
    im_height, im_width = image.shape

    hist_agg = np.zeros(shape=(256,))
    bins = None
    for mp_x, mp_y in points:
        bb_img = image[max(int(mp_y - (params.histogram_bb_size / 2)), 0) : min(int(mp_y + (params.histogram_bb_size / 2)), im_height),
                       max(int(mp_x - (params.histogram_bb_size / 2)), 0) : min(int(mp_x + (params.histogram_bb_size / 2)), im_width)]
        if bb_img.mean() < min_intensity:
          continue
        hist, bins = np.histogram(bb_img.ravel(), 256, [0, 256])
        hist_agg += hist
    if len(points) == 0:
      return np.array([]), np.array([]), 0

    total_freq = np.sum(hist_agg)
    hist_agg_prob = hist_agg / total_freq

    try:
        sternum_bins = bins[np.argwhere(hist_agg_prob > 0.2)]
    except:
        sternum_bins = []
    return reject_outliers(np.array(sternum_bins)), hist_agg, len(points)

def export_histogram(image, evolving_image, label, points, params, draw, save_to_file = True, min_intensity = 0.):
   bins, hist_agg, _ = get_hist_for_points(image, points, params, draw,
       min_intensity=min_intensity)
   if save_to_file:
       hist_agg_norm = hist_agg / np.sum(hist_agg)
       evolving_image.add_shared_plot(label, [hist_agg_norm], [""], xlabel="pixel intensity",
           xticks=np.arange(0, 255, step=10), yticks=np.arange(0, 1., step=0.1))
       evolving_image.add_shared_plot(f"{label}_CUMULATIVE", [np.cumsum(hist_agg_norm)], [""], xlabel="pixel intensity",
           xticks=np.arange(0, 255, step=10), yticks=np.arange(0, 1., step=0.1))
   return bins, hist_agg

class MostProbablePointsElements(NamedTuple):
    coords_most_probably_in_lung: Sequence[Tuple[int, int]]
    num_points_in_max_contour: int

class StructurePoints(NamedTuple):
    intensities: Sequence[float]
    hist: Sequence[float]
    sternum_points: int
    left_hist: Sequence[float]
    left_sternum_points: int
    right_hist: Sequence[float]
    right_sternum_points: int


class HoughSegmentator(BaseSegmentator):
  def __init__(self, images, image_paths, params):
    self._images = images
    self._image_paths = image_paths
    self._threshold_segmentator = SimpleSegmentator(images, image_paths, params)
    self._params = params

  def segment(self):
    for image, image_title in zip(self._images, self._image_paths):
      yield self.segment_one(image, image_title)

  @staticmethod
  def shoulder_cut_line_intensities(evolving_image, mp_points, params):
    image = evolving_image(EvolvingImage.Stage.CONTRAST_ENHANCED).copy()
    shoulder_line_left = evolving_image.shoulder_line_left
    shoulder_line_right = evolving_image.shoulder_line_right
    draw = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    shoulder_points = []
    shoulder_points_left = []
    shoulder_points_right = []
    mp_map_y_to_x = defaultdict(list)
    for mp_x, mp_y in mp_points:
      cv.circle(draw, (mp_x, mp_y), 2, (0, 255, 0))
      mp_map_y_to_x[mp_y].append(mp_x)
    for row, col in shoulder_line_left:
      if row in mp_map_y_to_x:
        for mp_x in mp_map_y_to_x[row]:
          if abs(mp_x - col) <= params.shoulder_cut_line_width:
            shoulder_points.append((mp_x, row))
            if mp_x <= col:
              shoulder_points_left.append((mp_x, row))
    for row, col in shoulder_line_right:
      if row in mp_map_y_to_x:
        for mp_x in mp_map_y_to_x[row]:
          if abs(mp_x - col) <= params.shoulder_cut_line_width:
            shoulder_points.append((mp_x, row))
            if mp_x <= col:
              shoulder_points_right.append((mp_x, row))
    labeled_points = {}
    bins, hist_agg = export_histogram(image, evolving_image, "SHOULDER_POINTS_HISTOGRAM", shoulder_points, params, draw,
        min_intensity=params.intensity_upper_threshold)
    bins_left, hist_agg_left = \
        export_histogram(image, evolving_image, "SHOULDER_POINTS_HISTOGRAM_LEFT", shoulder_points_left, params, draw,
            min_intensity=params.intensity_upper_threshold)
    bins_right, hist_agg_right = \
        export_histogram(image, evolving_image, "SHOULDER_POINTS_HISTOGRAM_RIGHT", shoulder_points_right, params, draw,
            min_intensity=params.intensity_upper_threshold)
    return StructurePoints(bins, hist_agg, shoulder_points,
                           hist_agg_left, shoulder_points_left,
                           hist_agg_right, shoulder_points_right)

  @staticmethod
  def sternum_intensities(evolving_image, mp_points, params):
    image = evolving_image(EvolvingImage.Stage.CONTRAST_ENHANCED).copy()
    sternum_line = evolving_image.split_line
    draw = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    sternum_points = []
    sternum_points_left = []
    sternum_points_right = []
    mp_map_y_to_x = defaultdict(list)
    for mp_x, mp_y in mp_points:
      cv.circle(draw, (mp_x, mp_y), 2, (0, 255, 0))
      mp_map_y_to_x[mp_y].append(mp_x)
    for row, col in sternum_line:
      if row in mp_map_y_to_x:
        for mp_x in mp_map_y_to_x[row]:
          if abs(mp_x - col) <= params.sternum_width:
            sternum_points.append((mp_x, row))
            if mp_x <= col:
              sternum_points_left.append((mp_x, row))
            else:
              sternum_points_right.append((mp_x, row))
    bins, hist_agg = export_histogram(image, evolving_image, "STERNUM_POINTS_HISTOGRAM", sternum_points, params, draw,
        min_intensity=params.intensity_upper_threshold)
    bins_left, hist_agg_left = \
        export_histogram(image, evolving_image, "STERNUM_POINTS_HISTOGRAM_LEFT", sternum_points_left, params, draw,
            min_intensity=params.intensity_upper_threshold)
    bins_right, hist_agg_right = \
        export_histogram(image, evolving_image, "STERNUM_POINTS_HISTOGRAM_RIGHT", sternum_points_right, params, draw,
            min_intensity=params.intensity_upper_threshold)


    return StructurePoints(bins, hist_agg, sternum_points,
                           hist_agg_left, sternum_points_left,
                           hist_agg_right, sternum_points_right)

  @staticmethod
  def mp_lung_point_intensities(evolving_image, mp_points, sternum_intensities, params, exclude_sternum_points = True,
                                exclude_sternum_intensities = False, sternum_points = None, exclude_shoulder_points = True,
                                shoulder_points = None, min_row = 0, max_row = None, min_col = 0, max_col = None, save_histograms = False,
                                save_image=False):
    image = evolving_image(EvolvingImage.Stage.CONTRAST_ENHANCED).copy()
    im_height, im_width = image.shape
    max_row = max_row if max_row is not None else im_height
    max_col = max_col if max_col is not None else im_width
    draw = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    split_line = evolving_image.split_line

    sternum_lower_th = min(sternum_intensities) if sternum_intensities else None
    mp_lung_points = []
    mp_lung_points_left = []
    mp_lung_points_right = []
    hist_agg = np.zeros(shape=(256,))
    bins = None
    if exclude_sternum_points and sternum_points is None:
        print("To exclude sternum points the sternum points need to be provided")
    if exclude_shoulder_points and shoulder_points is None:
        print("To exclude shoulder points the shoulder points need to be provided")
    split_line_map_y_to_x = defaultdict(list)
    for row, col in split_line:
      split_line_map_y_to_x[row].append(col)
    for mp_x, mp_y in mp_points:
        if exclude_sternum_points and sternum_points is not None and (mp_x, mp_y) in sternum_points:
            cv.circle(draw, (mp_x, mp_y), 1, (0, 0, 255))
            continue

        if exclude_shoulder_points and shoulder_points is not None and (mp_x, mp_y) in shoulder_points:
            cv.circle(draw, (mp_x, mp_y), 1, (0, 0, 255))
            continue
        bb_img = image[max(int(mp_y - (params.histogram_bb_size / 2)), 0) : min(int(mp_y + (params.histogram_bb_size / 2)), im_height),
                       max(int(mp_x - (params.histogram_bb_size / 2)), 0) : min(int(mp_x + (params.histogram_bb_size / 2)), im_width)]
        if mp_y < min_row or mp_y > max_row or mp_x < min_col or mp_x > max_col:
            cv.circle(draw, (mp_x, mp_y), 1, (255, 0, 0))
            continue

        point_intensity = bb_img.mean()

        if (bb_img <= params.intensity_lower_threshold).any():
          continue

        if (not exclude_sternum_intensities or sternum_intensities is None or \
            sternum_lower_th is None or point_intensity < sternum_lower_th):
            mp_lung_points.append((mp_x, mp_y))
            cv.circle(draw, (mp_x, mp_y), 2, (0, 255, 0))
            if mp_x < split_line_map_y_to_x[mp_y]:
              mp_lung_points_left.append((mp_x, mp_y))
            else:
              mp_lung_points_right.append((mp_x, mp_y))

    bins, hist_agg = export_histogram(image, evolving_image, "MP_LUNG_POINTS_HISTOGRAM", mp_lung_points, params, draw,
        save_to_file=save_histograms)
    bins_left, hist_agg_left = \
        export_histogram(image, evolving_image, "MP_LUNG_POINTS_HISTOGRAM_LEFT", mp_lung_points_left, params, draw,
            save_to_file=save_histograms)
    bins_right, hist_agg_right = \
        export_histogram(image, evolving_image, "MP_LUNG_POINTS_HISTOGRAM_RIGHT", mp_lung_points_right, params, draw,
            save_to_file=save_histograms)

    if save_image:
        evolving_image.add_image_in_stage(EvolvingImage.Stage.SHOULDER_STERNUM_AND_LUNG_MP_POINTS, draw)
        bins_axis_y, intensities_axis_y = get_pixel_intensity_at_axis_graph(image, mp_lung_points, params, axis = 0)
        bins_axis_y_left, intensities_axis_y_left = get_pixel_intensity_at_axis_graph(image, mp_lung_points_left, params, axis = 0)
        bins_axis_y_right, intensities_axis_y_right = get_pixel_intensity_at_axis_graph(image, mp_lung_points_right, params, axis = 0)
        evolving_image.add_shared_plot("AXIS_Y_TOP_TO_BOTTOM_INTENSITIES", [intensities_axis_y], [""], xlabel="pixel",
            xticks=bins_axis_y, yticks=np.arange(0, max(intensities_axis_y), step=1.), data_x=bins_axis_y)
        evolving_image.add_shared_plot("AXIS_Y_TOP_TO_BOTTOM_INTENSITIES_LEFT", [intensities_axis_y_left], [""], xlabel="pixel",
            xticks=bins_axis_y_left, yticks=np.arange(0, max(intensities_axis_y_left), step=1.), data_x=bins_axis_y_left)
        evolving_image.add_shared_plot("AXIS_Y_TOP_TO_BOTTOM_INTENSITIES_RIGHT", [intensities_axis_y_right], [""], xlabel="pixel",
            xticks=bins_axis_y_right, yticks=np.arange(0, max(intensities_axis_y_right), step=1.), data_x=bins_axis_y_right)

    return StructurePoints(bins, hist_agg, mp_lung_points,
                           hist_agg_left, mp_lung_points_left,
                           hist_agg_right, mp_lung_points_right)

  @staticmethod
  def find_mp_lung_points(params, evolving_image):
    enhanced_image = evolving_image(EvolvingImage.Stage.CONTRAST_ENHANCED).copy()
    hough_lines_image = enhanced_image.copy()
    edges = cv.Canny(enhanced_image, params.first_canny_threshold, params.second_canny_threshold,
                     apertureSize = params.aperture_size)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.EDGES, edges)
    minLineLength = params.hough_min_line_length
    maxLineGap = params.hough_max_line_gap
    lines = cv.HoughLinesP(edges, cv.HOUGH_PROBABILISTIC, 3 * np.pi / 180, params.hough_accumulator_threshold,
                           minLineLength, maxLineGap)
    y_per_x_coords = defaultdict(list)
    for x in range(0, len(lines)):
        for x1,y1,x2,y2 in lines[x]:
            y_per_x_coords[x1].append(y1)
            y_per_x_coords[x2].append(y2)
            pts = np.array([[x1, y1 ], [x2 , y2]], np.int32)
            cv.polylines(hough_lines_image, [pts], True, (0, 255, 0))
    evolving_image.add_image_in_stage(EvolvingImage.Stage.HOUGH_LINES, hough_lines_image)
    for x_coord, y_coords in HoughSegmentator.coords_most_probably_in_lung(y_per_x_coords, params):
      for y_coord in y_coords:
        yield x_coord, y_coord

  def segment_one(self, evolving_image, image_title, mp_lung_points,
                  intensity_lower_threshold = None, intensity_upper_threshold = None):
     
    image = evolving_image(EvolvingImage.Stage.CONTRAST_ENHANCED).copy()

    mp_lung_points_image = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    mp_lung_points_only_image = np.zeros(image.shape, dtype = "uint8")
    segmented_image, contours, contour_img = \
        self._threshold_segmentator.segment_one(image, image_title,
                                                intensity_lower_threshold=intensity_lower_threshold \
                                                    if intensity_lower_threshold is not None else \
                                                    self._params.intensity_lower_threshold,
                                                intensity_upper_threshold=intensity_upper_threshold \
                                                    if intensity_upper_threshold is not None else \
                                                    self._params.intensity_upper_threshold)
    m_tr = MorphologicalTransformer(5, 1, 5, 1)
    eroded = m_tr.erode(segmented_image)
    _, contours, _ = cv.findContours(eroded, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.THRESHOLDED, segmented_image)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.INITIAL_CONTOURS, contour_img, contours)

    mp_lung_points = mp_lung_points if mp_lung_points is not None \
        else self.find_mp_lung_points(self._params, evolving_image)
    num_points_per_contour = defaultdict(int)
    coords_most_probably_in_lung = []
    for x_coord, y_coord in mp_lung_points:
      for i, cnt in enumerate(contours):
        if cv.contourArea(cnt) <= self._params.min_contour_area:
          continue
        if cv.pointPolygonTest(cnt, (x_coord, y_coord), True) >= 0:
          num_points_per_contour[i] += 1
      coords_most_probably_in_lung.append((x_coord, y_coord))
      cv.circle(mp_lung_points_image, (x_coord, y_coord), 2, (0, 255, 0))
      mp_lung_points_only_image[y_coord, x_coord] = 255
    if not num_points_per_contour:
      contours_to_keep = np.array([max(contours, key=cv.contourArea)])
      num_points_in_max_contour = 0
    else:
      contour_to_keep_i = max(num_points_per_contour, key=num_points_per_contour.get)
      num_points_in_max_contour = num_points_per_contour[contour_to_keep_i]
      contours_to_keep = np.array([contours[contour_to_keep_i]])
    c_image =  np.zeros(image.shape, np.uint8)
    evolving_image.add_image_in_stage_with_contours(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS,
                                                    c_image, contours_to_keep)
    evolving_image.add_image_in_stage_with_contours(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS_ON_IMAGE,
                                                    image, contours_to_keep)
    filtered_segmented_image = np.zeros(image.shape, np.uint8)
    cv.drawContours(filtered_segmented_image, contours_to_keep, -1, (255, 0, 0),
                    thickness=cv.FILLED)
    filtered_segmented_image1 = m_tr.dilate(filtered_segmented_image)
    _, contours_to_keep, _ = cv.findContours(filtered_segmented_image, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.SEGMENTED, filtered_segmented_image)

    evolving_image.add_image_in_stage(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS, mp_lung_points_image,
                                      MostProbablePointsElements(coords_most_probably_in_lung,
                                                                 num_points_in_max_contour))

    evolving_image.add_image_in_stage(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS_ONLY, mp_lung_points_only_image)

    
    return filtered_segmented_image, contours_to_keep

  @staticmethod
  def coords_most_probably_in_lung(y_per_x_coords, params):
    for x_coord, y_coords in y_per_x_coords.items():
      if len(y_coords) < params.min_parallel_lines: 
        continue
      y_coords_sorted = sorted(y_coords)
      y_coords_diff = np.diff(y_coords_sorted)
      std = np.std(y_coords_diff)
      if std <= params.parallel_line_distance_variance_thresh:
        yield x_coord, y_coords

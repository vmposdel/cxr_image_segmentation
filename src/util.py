import cv2 as cv
import numpy as np
from math import sqrt
from matplotlib import pyplot as plt
from numpy.linalg import norm
from skimage.draw import line
from sklearn.cluster import KMeans
from sklearn.neighbors import KDTree
from typing import NamedTuple, Optional, Tuple
from src.config import VISUALIZATION

class BBBox(NamedTuple):
    upper_left: Tuple[float, float]
    bottom_right: Tuple[float, float]

def histogram_equalization(img):
    equ = cv.equalizeHist(img)
    if VISUALIZATION:
        res = np.hstack((img, equ))
        cv.imshow("Before / After Equalization", res)
        cv.waitKey()
    return equ

def log_transform(img, c):
    g = c * (np.log(1 + np.float32(img)))
    return g

def contrast_limited_adaptive_histogram_equalization(img, clip_limit, tile_grid_size):
    clahe = cv.createCLAHE(clipLimit=clip_limit, tileGridSize=(tile_grid_size, tile_grid_size))
    equ = clahe.apply(img)
    if VISUALIZATION:
        res = np.hstack((img, equ))
        cv.imshow("Before / After Clahe Equalization", res)
        cv.waitKey()
    return equ

def hough_line_transform(img):
    edges = cv.Canny(img, 50, 150, apertureSize = 3)
    minLineLength = 100
    maxLineGap = 10
    lines = cv.HoughLinesP(edges, 1, np.pi/180, 100, minLineLength, maxLineGap)
    for x1, y1, x2, y2 in lines[0]:
        cv.line(img, (x1, y1),(x2, y2),(0, 255, 0), 2)

def gabor_filter(img):
    def build_filters():
        filters = []
        ksize = 31
        for theta in np.arange(0, np.pi, np.pi / 16):
            kern = cv.getGaborKernel((ksize, ksize), 4.0, theta, 10.0, 0.5, 0, ktype=cv.CV_32F)
            kern /= 1.5 * kern.sum()
            filters.append(kern)
        return filters
 
    def process(img, filters):
        accum = np.zeros_like(img)
        for kern in filters:
            fimg = cv.filter2D(img, cv.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
        return accum
    filters = build_filters()
    res1 = process(img, filters)
    if VISUALIZATION:
      cv.imshow('result', res1)
      cv.waitKey(0)

def point_distance(point1, point2):
    return sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

def nearest_points(points, max_distance):
    print(points)
    tree = KDTree(points, leaf_size=2)
    print(tree.query_radius(points[0], r=max_distance, count_only=True))

def get_line(start, end):
    start_x = start[0]
    start_y = start[1]
    end_x = end[0]
    end_y = end[1]
    discrete_line = list(zip(*line(*(start_x, start_y), *(end_x, end_y))))
    return discrete_line[0], discrete_line[-1], discrete_line

def get_line_full(start, end, max_x, max_y, min_x = 0, min_y = 0):
    start_x = start[0]
    start_y = start[1]
    end_x = end[0]
    end_y = end[1]
    discrete_line = list(zip(*line(*(start_x, start_y), *(end_x, end_y))))
    min_intersect_x, min_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                         ((min_x, min_y), (min_x, max_y)))
    assert(min_intersect_x is None or min_intersect_x == min_x)
    max_intersect_x, max_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                        ((max_x, min_y), (max_x, max_y)))
    assert(max_intersect_x is None or max_intersect_x == max_x)

    if min_intersect_x is None:
      assert max_intersect_x is None
      min_intersect_x, min_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                           ((min_x, min_y), (max_x, min_y)))
      max_intersect_x, max_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                           ((min_x, max_y), (max_x, max_y)))
    else:
      if min_intersect_y < min_y:
          min_intersect_x, min_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                               ((min_x, min_y), (max_x, min_y)))
      elif min_intersect_y > max_y:
          min_intersect_x, min_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                               ((min_x, max_y), (max_x, max_y)))
      assert min_x <= min_intersect_x <= max_x
      if max_intersect_y < min_y:
          max_intersect_x, max_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                               ((min_x, min_y), (max_x, min_y)))
      elif max_intersect_y > max_y:
          max_intersect_x, max_intersect_y = line_intersection((discrete_line[0], discrete_line[-1]),
                                                             ((min_x, max_y), (max_x, max_y)))
    assert min_x <= min_intersect_x <= max_x
    return get_line((min_intersect_x, min_intersect_y), (max_intersect_x, max_intersect_y))

def get_angle(p0, p1, p2):
    ''' compute angle (in degrees) for p0p1p2 corner
    Inputs:
        p0,p1,p2 - points in the form of [x,y]
    '''
    v0 = np.array(p0) - np.array(p1)
    v1 = np.array(p2) - np.array(p1)

    angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
    return np.degrees(angle)

def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       print("Line intersection not found.")
       return None, None

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return int(x), int(y)

def sliding_window(image, step_size, window_size):
  # slide a window across the image
  for y in range(0, image.shape[0], step_size):
    for x in range(0, image.shape[1], step_size):
      # yield the current window
      yield (x, y, image[y : y + window_size[1], x : x + window_size[0]])


def select_n_clusters(data):
  Sum_of_squared_distances = []
  K = range(1,15)
  for k in K:
    km = KMeans(n_clusters=k)
    km = km.fit(data)
    Sum_of_squared_distances.append(km.inertia_)
  plt.plot(K, Sum_of_squared_distances, 'bx-')
  plt.xlabel('k')
  plt.ylabel('Sum_of_squared_distances')
  plt.title('Elbow Method For Optimal k')
  plt.show()

def kmeans_clustering(data):
  data = np.array(data)
  n_clusters = 12
  kmeans = KMeans(n_clusters = n_clusters)
  kmeans.fit(data)
  return kmeans

def get_filtered_connected_component_labels(labels, filter_out_labels = []):
  for l in filter_out_labels:
    labels[labels == l] = 0

  return labels

def get_connected_components_image(labels, filter_out_labels = []):
  # Map component labels to hue val
  labels = get_filtered_connected_component_labels(labels, filter_out_labels)
  label_hue = np.uint8(179 * labels / np.max(labels))
  blank_ch = 255 * np.ones_like(label_hue)
  labeled_img = cv.merge([label_hue, blank_ch, blank_ch])

  # cvt to BGR for display
  labeled_img = cv.cvtColor(labeled_img, cv.COLOR_HSV2BGR)

  # set bg label to black
  labeled_img[label_hue==0] = 0
  return labeled_img, labels

def get_connected_component_points(labels, label):
  points = []
  for point in np.argwhere(labels == label):
    points.append(np.array([point[1], point[0]]))
  return np.array(points)

def dist_point_to_contour(point, contour):
  points = [(c_point[0][0], c_point[0][1]) for c_point in contour]
  points_arr = np.asarray(points)
  dist_2 = np.sum((points_arr - point) ** 2, axis=1)
  return points, dist_2

def min_dist_point_to_line(line_point1, line_point2, point):
  d = norm(np.cross(line_point2 - line_point1, line_point1 - point)) / norm(line_point2-line_point1)
  return d
  
def closest_contour_point(point, contour):
  points, dist_2 = dist_point_to_contour(point, contour)
  min_index = np.argmin(dist_2)
  return min_index, sqrt(dist_2[min_index]), points[min_index]

def farthest_contour_point(point, contour):
  points, dist_2 = dist_point_to_contour(point, contour)
  max_index = np.argmax(dist_2)
  return max_index, sqrt(dist_2[max_index]), points[max_index]

def intensity_stats_on_line(point1, point2, image, bb_size=1, intensity_at_edges = {}):
  class Stats(NamedTuple):
    mean: float
    variance: float
    maximum: float
    minimum: float
    max_diff: Optional[float]
    diffs: np.array
    points_in_line: np.array
  im_height, im_width = image.shape
  _, _, points_in_line = get_line(point1, point2)
  intensity = []
  save_points_in_line = []
  for point in points_in_line:
      save_points_in_line.append(point)
      if point in intensity_at_edges:
        intensity.append(intensity_at_edges[point])
        continue
      if bb_size == 1:
        intensity.append(image[point[1]][point[0]])
      else:
        assert(bb_size % 2 == 0)
        x_coord = point[0]
        y_coord = point[1]
        bb_r = int(bb_size / 2)
        bb = image[max(0, y_coord - bb_r):min(im_height, y_coord + bb_r),
             max(0, x_coord - bb_r):min(im_width, x_coord + bb_r)] 
        intensity.append(cv.mean(bb)[0])
  if len(intensity) == 0:
    return None
  diff = np.diff(intensity)
  return Stats(mean=sum(intensity) / len(intensity), variance=np.var(intensity),
               maximum=max(intensity), minimum=min(intensity), max_diff=max(diff) if len(diff) > 0 else None,
               diffs=diff, points_in_line=np.array(save_points_in_line))

def intensity_stats_around_point_in_contour(point, contour, image):
  im_height, im_width = image.shape
  bb_image =  np.zeros(image.shape, np.uint8)
  cnt_image =  np.zeros(image.shape, np.uint8)
  bb_size = 15
  x_coord = point[0]
  y_coord = point[1]
  bb_image[max(0, y_coord - bb_size):min(im_height, y_coord + bb_size),
           max(0, x_coord - bb_size):min(im_width, x_coord + bb_size)] = 255
  cnt_image = cv.drawContours(cnt_image, [contour], -1, (255, 0, 0), thickness=cv.FILLED)

  mask = cv.bitwise_and(bb_image, bb_image, mask = cnt_image)
  return cv.mean(image, mask=mask)[0]

def first_non_zero(image, axis=0, invalid_val=-1):
  mask = image != 0
  return np.where(mask.any(axis=axis), mask.argmax(axis=axis), invalid_val)

def last_non_zero(image, axis=0, invalid_val=-1):
  mask = image != 0
  val = image.shape[axis] - np.flip(mask, axis=axis).argmax(axis=axis) - 1
  return np.where(mask.any(axis=axis), val, invalid_val)

def find_convexity_defects(contour):
  hull = cv.convexHull(contour, returnPoints=False)
  defects = cv.convexityDefects(contour, hull)
  if defects is None:
    return

  for i in range(defects.shape[0]):
    yield defects[i, 0]

def iqr_outlier_detector(data, outlierConstant):
  upper_quartile = np.percentile(data, 75)
  lower_quartile = np.percentile(data, 25)
  IQR = (upper_quartile - lower_quartile) * outlierConstant
  quartile_set = (lower_quartile - IQR, upper_quartile + IQR)
  result_list = []
  for i, y in enumerate(data.tolist()):
    if y < quartile_set[0] or y > quartile_set[1]:
       result_list.append(i)
  return result_list

def z_score_outlier_detector(data):
  threshold = 1
  mean_1 = np.mean(data)
  std_1 =np.std(data)
  outliers = []
  
  for i, y in enumerate(data):
      print("Out ", i, y, mean_1, std_1)
      z_score = (y - mean_1) / std_1 
      if np.abs(z_score) > threshold:
          outliers.append(i)
  return outliers

def modified_z_score_outlier_detector(data):
  threshold = 1.

  median_1 = np.median(data)
  median_absolute_deviation_1 = np.median([np.abs(y - median_1) for y in data])

  outliers = []
  
  for i, y in enumerate(data):
      modified_z_score = 0.6745 * (y - median_1) / median_absolute_deviation_1
      if np.abs(modified_z_score) > threshold:
          outliers.append(i)
  return outliers

def bb_around_point(point, image, offset):
  im_height, im_width = image.shape
  bb = image[max(0, point[1] - offset):min(im_height, point[1] + offset),
             max(0, point[0] - offset):min(im_width, point[0] + offset)]
  return bb

def reject_outliers(data, m=2):
    return data[abs(data - np.mean(data)) < m * np.std(data)]

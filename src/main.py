import argparse
import cv2 as cv
import csv
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import os
from bisect import bisect_left
from jsrt_parser.jsrt import Jsrt, JsrtImage
from typing import NamedTuple

from src.region_transformer import RegionTransformer
from src.simple_segmentator import SimpleSegmentator
from src.visualization_utils import EvolvingImage
from src.wavelet_transformer import WaveletTransformer
from src.hough_segmentator import HoughSegmentator
from src.border_line_growable_hough_segmentator import BorderLineGrowableHoughSegmentator
from src.haar_growable_hough_segmentator import HaarGrowableHoughSegmentator
from src.segmentation_evaluator import SegmentationEvaluator, EvaluationImage
from src.symmetry_detector import SymmetryDetector
from src.montgomery import Montgomery
from src.config import VISUALIZATION_GT, default_params
from src.util import histogram_equalization, contrast_limited_adaptive_histogram_equalization, \
        hough_line_transform, gabor_filter, log_transform
from src.visualization_utils import visualize_image, visualize_two

NUM_EVALUATION_IMAGES = 138

class Processor:
    def __init__(self, segmentator, images, image_paths, gt_images, image_title_gt_image_mapping,
                 region_transformer, gt_stats, params = default_params, experiment_folder_name = None,
                 compare_with_file = None, adaptive_thresholding = False):
        self._segmentator = segmentator
        self._images = images
        self._image_paths = image_paths
        self._gt_images = gt_images
        self._title_gt_image_mapping = image_title_gt_image_mapping
        self._region_transformer = region_transformer
        self._params = params
        self._experiment_folder_name = experiment_folder_name
        self._compare_with_file = compare_with_file
        self._adaptive_thresholding = adaptive_thresholding
        self._gt_stats = gt_stats

    def get_image_split_in_two_by_line(self, image, line):
        height, width = image.shape
        left_image = np.zeros((height, width, 1), np.uint8)
        right_image = np.zeros((height, width, 1), np.uint8)
        for row, col in line:
            left_image[row, 0 : col - self._params.symmetry_line_column_shift] = 255
            right_image[row, col + self._params.symmetry_line_column_shift : width] = 255
        return (cv.bitwise_and(image, image, mask = left_image),
                cv.bitwise_and(image, image, mask = right_image))

    @staticmethod
    def combine_binary_left_and_right_parts(img_left, img_right):
        return cv.bitwise_or(img_left, img_right)

    def segmented_images(self):
        image_thresholds = {}
        all_hist_agg = np.zeros(shape=(256,))
        check_list = range(0, NUM_EVALUATION_IMAGES)
        for image, image_path in zip(self._images, self._image_paths):
            orig_image = image.copy()
            image_title = os.path.basename(image_path)
            img_number = self._title_gt_image_mapping[image_title]
            if img_number not in check_list:
              continue
            im_height, im_width = image.shape
            symmetry_line = SymmetryDetector.run_for_image(image)
            line_slope = (symmetry_line[-1][0] - symmetry_line[0][0]) / (symmetry_line[-1][1] - symmetry_line[0][1])
            image, shoulder_line_left = \
                    SymmetryDetector.cut_triangle_up_to_pixel(image, symmetry_line[0][1], line_slope,
                                                              0 , self._params.triangle_end_y, 0.)
            image, shoulder_line_right = \
                    SymmetryDetector.cut_triangle_up_to_pixel(image, symmetry_line[0][1], line_slope,
                                                              image.shape[1] - 1, self._params.triangle_end_y, 0.)
            left_image, right_image = self.get_image_split_in_two_by_line(image, symmetry_line)
            evolving_left_image = EvolvingImage(left_image, is_left_part=True, img_number=img_number,
                                                split_line=symmetry_line)
            evolving_right_image = EvolvingImage(right_image, is_left_part=False, img_number=img_number,
                                                 split_line=symmetry_line)
            evolving_left_image.add_image_in_stage(EvolvingImage.Stage.CONTRAST_ENHANCED, 
                    contrast_limited_adaptive_histogram_equalization(left_image, self._params.clip_limit,
                                                                     self._params.tile_grid_size))
            evolving_left_image.add_image_in_stage(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED, 
                    contrast_limited_adaptive_histogram_equalization(left_image, self._params.harris_clahe_clip_limit,
                                                                     self._params.tile_grid_size))
            evolving_right_image.add_image_in_stage(EvolvingImage.Stage.CONTRAST_ENHANCED, 
                    contrast_limited_adaptive_histogram_equalization(right_image, self._params.clip_limit,
                                                                     self._params.tile_grid_size))

            evolving_right_image.add_image_in_stage(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED, 
                    contrast_limited_adaptive_histogram_equalization(right_image, self._params.harris_clahe_clip_limit,
                                                                     self._params.tile_grid_size))


            evolving_full_image = EvolvingImage(orig_image, is_left_part=False, img_number=img_number,
                                                split_line=symmetry_line,
                                                shoulder_line_left=shoulder_line_left,
                                                shoulder_line_right=shoulder_line_right,
                                                is_full_image=True)
            mp_lung_points = None
            lung_upper_threshold_left = lung_upper_threshold_right = None
            if True:
                enhanced_image = contrast_limited_adaptive_histogram_equalization(orig_image, self._params.clip_limit,
                                                                                  self._params.tile_grid_size)
                evolving_full_image.add_image_in_stage(EvolvingImage.Stage.CONTRAST_ENHANCED, 
                        contrast_limited_adaptive_histogram_equalization(orig_image, self._params.clip_limit,
                                                                         self._params.tile_grid_size))
                left_mp_points = list(HoughSegmentator.find_mp_lung_points(self._params, evolving_left_image))
                right_mp_points = list(HoughSegmentator.find_mp_lung_points(self._params, evolving_right_image))

                mp_points = list(HoughSegmentator.find_mp_lung_points(self._params, evolving_left_image)) + \
                        list(HoughSegmentator.find_mp_lung_points(self._params, evolving_right_image))
                sternum_points = HoughSegmentator.sternum_intensities(evolving_full_image, left_mp_points + right_mp_points, self._params)
                sternum_intensities = sternum_points.intensities
                shoulder_points = HoughSegmentator.shoulder_cut_line_intensities(evolving_full_image, left_mp_points + right_mp_points, self._params)
                mp_lung_points_bottom = HoughSegmentator.mp_lung_point_intensities(evolving_full_image, left_mp_points + right_mp_points,
                        None, self._params, sternum_points=sternum_points.sternum_points, shoulder_points=shoulder_points.sternum_points,
                        min_row=im_height / 5, save_histograms=True)

                all_hist_agg += mp_lung_points_bottom.left_hist + mp_lung_points_bottom.right_hist

                left_hist_norm = mp_lung_points_bottom.left_hist / np.sum(mp_lung_points_bottom.left_hist)
                right_hist_norm = mp_lung_points_bottom.right_hist / np.sum(mp_lung_points_bottom.right_hist)
                evolving_full_image.add_shared_plot("MP_LUNG_POINTS_BOTH_SIDE_HISTOGRAM",
                                                    [left_hist_norm, right_hist_norm],
                                                    ["Right lung prob", "Left lung prob"], xlabel="pixel intensity",
                                                    show_legend=True, xticks=np.arange(0, 255, step=10), yticks=np.arange(0, 1., step=0.1))

                
                evolving_full_image.add_shared_plot("MP_LUNG_POINTS_BOTH_SIDE_CUM_HISTOGRAM",
                                                    [np.cumsum(left_hist_norm), np.cumsum(right_hist_norm)],
                                                    ["Right lung prob", "Left lung prob"], xlabel="pixel intensity",
                                                    show_legend=True, xticks=np.arange(0, 255, step=10), yticks=np.arange(0, 1., step=0.1))

                mp_lung_points = HoughSegmentator.mp_lung_point_intensities(evolving_full_image, left_mp_points + right_mp_points,
                        None, self._params, sternum_points=sternum_points.sternum_points, shoulder_points=shoulder_points.sternum_points,
                        save_image=True)


            image_thresholds[img_number] = (lung_upper_threshold_left, lung_upper_threshold_right)

            sternum_intensities = None
            if self._adaptive_thresholding:
                sternum_points = HoughSegmentator.sternum_intensities(enhanced_image, mp_points, self._params)
                sternum_intensities = sternum_points.intensities

            left_segmented_image, contours_left = \
                    self._segmentator.segment_one(evolving_left_image, image_path,
                            mp_lung_points=mp_lung_points.left_sternum_points \
                                    if mp_lung_points is not None else None,
                            intensity_upper_threshold=lung_upper_threshold_left)
            right_segmented_image, contours_right = \
                    self._segmentator.segment_one(evolving_right_image, image_path,
                            mp_lung_points=mp_lung_points.right_sternum_points \
                                    if mp_lung_points is not None else None,
                            intensity_upper_threshold=lung_upper_threshold_right)


            if self._region_transformer is not None:

              grown_contours = []    
              for cnt in contours_left:
                  grown_contours.append(
                          self._region_transformer.smoothen_convex_hull(
                              cnt, left_segmented_image, evolving_left_image, self._params))
              contours_left = grown_contours.copy()

              left_segmented_image = np.zeros(left_segmented_image.shape, np.uint8)
              left_segmented_image = cv.drawContours(left_segmented_image, contours_left, -1, (255, 0, 0),
                                                     thickness=cv.FILLED)
              grown_contours = []    
              for cnt in contours_right:
                  grown_contours.append(
                          self._region_transformer.smoothen_convex_hull(
                              cnt, right_segmented_image, evolving_right_image, self._params))
              contours_right = grown_contours.copy()
              right_segmented_image = np.zeros(right_segmented_image.shape, np.uint8)
              right_segmented_image = cv.drawContours(right_segmented_image, contours_right, -1, (255, 0, 0),
                                                      thickness=cv.FILLED)

            left_color_image = cv.cvtColor(left_image, cv.COLOR_GRAY2RGB)
            left_final_img_with_contours = cv.drawContours(left_color_image, contours_left, -1, (0, 0, 255),
                                                           thickness=3)
            evolving_left_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_ON_IMAGE,
                                          left_final_img_with_contours)
            right_color_image = cv.cvtColor(right_image, cv.COLOR_GRAY2RGB)
            right_final_img_with_contours = cv.drawContours(right_color_image, contours_right, -1, (0, 0, 255),
                                                           thickness=3)
            evolving_right_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_ON_IMAGE,
                                          right_final_img_with_contours)
            
            image_title = os.path.basename(image_path)

            left_gt_contour = self._gt_images[self._title_gt_image_mapping[image_title]].left_part_contours
            left_final_img_with_gt = cv.drawContours(left_final_img_with_contours, left_gt_contour, -1, (0, 255, 0),
                                                     thickness=1)
            evolving_left_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_ON_IMAGE_WITH_GT,
                                                   left_final_img_with_gt)
            right_gt_contour = self._gt_images[self._title_gt_image_mapping[image_title]].right_part_contours
            right_final_img_with_gt = cv.drawContours(right_final_img_with_contours, right_gt_contour, -1, (0, 255, 0),
                                                      thickness=1)
            evolving_right_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_ON_IMAGE_WITH_GT,
                                                    right_final_img_with_gt)

            evolving_left_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_FILLED,
                                                    left_segmented_image)
            evolving_right_image.add_image_in_stage(EvolvingImage.Stage.FINAL_CONTOURS_FILLED,
                                                    right_segmented_image)
            if self._experiment_folder_name is not None:
                evolving_full_image.save_all(self._experiment_folder_name)
                evolving_left_image.save_all(self._experiment_folder_name)
                evolving_right_image.save_all(self._experiment_folder_name)
            yield EvaluationImage(left_segmented_image, right_segmented_image,
                                  contours_left, contours_right), \
                                          self._title_gt_image_mapping[image_title]

        if self._experiment_folder_name is not None:
            w = csv.writer(open(f"{self._experiment_folder_name}/img_names.csv", "w"))
            for key, val in self._title_gt_image_mapping.items():
                w.writerow([key, val])
            w = csv.writer(open(f"{self._experiment_folder_name}/img_thresholds.csv", "w"))
            for key, val in image_thresholds.items():
                w.writerow([key, val[0], val[1]])

            all_hist_agg /=  np.sum(all_hist_agg)

            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            plt.xlabel("pixel_intensity");
            ax.set_xticks(np.arange(0, 255, step=10), minor=True)
            ax.set_yticks(np.arange(0, 1., step=0.1), minor=True)
            ax.grid(which='minor', alpha=0.75)
            plt.plot(np.cumsum(all_hist_agg))
            plt.savefig(f"{self._experiment_folder_name}/intensity_prob_hist.png")
            plt.clf()
            

    def run(self):
        segmented_images = []
        mapping_gt_result = {}
        for evaluation_image, gt_image in self.segmented_images():
            segmented_images.append(evaluation_image)
            mapping_gt_result[gt_image] = len(segmented_images) - 1
        evaluator = SegmentationEvaluator(self._gt_images, segmented_images, self._image_paths,
                                          mapping_gt_result)
        if self._experiment_folder_name is not None:
          evaluator.write_scores_to_file(f"{self._experiment_folder_name}/scores.csv")

        if self._compare_with_file is not None:
          evaluator.compare_with_scores_in_file(self._compare_with_file)

        return evaluator.jaccard_similarity_coefficient(True), evaluator.dices_coefficient() 


class GTStats(NamedTuple):
    avg_intensity: float
    avg_min_x_l: int
    avg_min_y_l: int
    avg_max_x_l: int
    avg_max_y_l: int
    avg_min_x_r: int
    avg_min_y_r: int
    avg_max_x_r: int
    avg_max_y_r: int

def get_gt_stats(images, image_paths, title_gt_image_mapping, gt_images, params):
    def get_extreme_points(cnt):
        leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
        rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
        topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
        bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
        return leftmost[0], topmost[1], rightmost[0], bottommost[1]
    sum_val = 0
    l_sum_min_x = l_sum_min_y = l_sum_max_x = l_sum_max_y = 0
    r_sum_min_x = r_sum_min_y = r_sum_max_x = r_sum_max_y = 0
    for image, image_path in zip(images, image_paths):
        enhanced = \
                contrast_limited_adaptive_histogram_equalization(image, params.clip_limit,
                                                                 params.tile_grid_size)
        image_title = os.path.basename(image_path)
        img_number = title_gt_image_mapping[image_title]
        gt_image = gt_images[img_number]
        sum_val += cv.mean(enhanced, mask = gt_image.left_part)[0] + \
                cv.mean(enhanced, mask = gt_image.right_part)[0]
        l_cnts = gt_image.left_part_contours
        assert(len(l_cnts) == 1)
        r_cnts = gt_image.right_part_contours
        assert(len(r_cnts) == 1)
        min_x, min_y, max_x, max_y = get_extreme_points(l_cnts[0])
        l_sum_min_x += min_x
        l_sum_min_y += min_y
        l_sum_max_x += max_x
        l_sum_max_y += max_y
        min_x, min_y, max_x, max_y = get_extreme_points(r_cnts[0])
        r_sum_min_x += min_x
        r_sum_min_y += min_y
        r_sum_max_x += max_x
        r_sum_max_y += max_y
    return GTStats(avg_intensity=sum_val / len(images),
            avg_min_x_l=l_sum_min_x / len(images), avg_min_y_l=l_sum_min_y / len(images),
            avg_max_x_l=l_sum_max_x / len(images), avg_max_y_l=l_sum_max_y / len(images),
            avg_min_x_r=r_sum_min_x / len(images), avg_min_y_r=r_sum_min_y / len(images),
            avg_max_x_r=r_sum_max_x / len(images), avg_max_y_r=r_sum_max_y / len(images))


def raw_to_png(raw_image_name, output_dir, out_filename):
    image = JsrtImage().load_from_file(raw_image_name)
    #image.display(nodule_marking=False)
    fig = plt.figure()
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    if VISUALIZATION_GT:
        ax.imshow(np.invert(image.image), cmap=plt.get_cmap('gray'))
    print("Saving figure ", out_filename)
    fig.savefig(output_dir + out_filename)
    plt.close(fig)

def load_jsrt_image_and_meta(im_dir, clinical_info_dir, manual_masks_dir):
    jsrt = Jsrt().load_images(im_dir, clinical_info_dir)
    gt = jsrt.load_gt_images(manual_masks_dir)
    return jsrt

def load_montgomery_image_and_meta(im_dir, clinical_info_dir, manual_masks_dir, num_of_images = None):
    montgomery = Montgomery().load_images(im_dir, clinical_info_dir, num_of_images)
    gt = montgomery.load_gt_images(manual_masks_dir)
    return montgomery

def run_experiment(args, params):
    experiment_folder_name = args.experiment_name if args.experiment_name else ""
    if args.jsrt:
        data_dir = "/home/v/Documents/crx_segmentation/All247images/All247images/"
        clinical_info_dir = "/home/v/Documents/crx_segmentation/All247images/ClinicalInformation/"
        manual_masks_dir = "/home/v/Documents/crx_segmentation/All247images/All247images/masks/"
        if args.raw:
            for filename in os.listdir(data_dir):
                if not filename.endswith(".IMG"):
                    continue
                raw_to_png(data_dir + filename, data_dir + "png-invert/", filename + ".png")
        jsrt = load_jsrt_image_and_meta(data_dir + "png-invert/", clinical_info_dir, manual_masks_dir)
        images = [im_meta.image for im_meta in jsrt.get_images(num_of_images=NUM_EVALUATION_IMAGES)]
        gt_images = []
        image_paths = [im_meta.image_path for im_meta in jsrt.get_images(num_of_images=NUM_EVALUATION_IMAGES)]
        image_title_gt_image_mapping = jsrt.get_title_gt_image_mapping()
        experiment_folder_name += "jsrt_"
    elif args.montgomery:
        data_dir = "/home/v/Documents/crx_segmentation/MontgomerySet/CXR_png/"
        clinical_info_dir = "/home/v/Documents/crx_segmentation/MontgomerySet/ClinicalReadings/"
        manual_masks_dir = "/home/v/Documents/crx_segmentation/MontgomerySet/ManualMask/"
        montgomery = load_montgomery_image_and_meta(data_dir, clinical_info_dir, manual_masks_dir,
                                                    num_of_images=NUM_EVALUATION_IMAGES)
        images = [im_meta.image for im_meta in montgomery.get_images(num_of_images=NUM_EVALUATION_IMAGES)]
        gt_images = [gt_img for gt_img in montgomery.get_gt_images(num_of_images=NUM_EVALUATION_IMAGES)]
        image_paths = [im_meta.image_path for im_meta in montgomery.get_images(num_of_images=NUM_EVALUATION_IMAGES)]
        image_title_gt_image_mapping = montgomery.get_title_gt_image_mapping()
        experiment_folder_name += "montgomery_"
    else:
        raise ValueError("Please select which dataset to use")

    segmentator = None
    convex_hull_allow_split = True
    if args.simple:
        segmentator = SimpleSegmentator(images, image_paths, params)
        experiment_folder_name += "simple"
    elif args.wavelet:
        wavelet_t = WaveletTransformer(images, image_paths)
        for hh in wavelet_t.compute_multilevel_discrete_wavelet_transform():
            pass
        experiment_folder_name += "wavelet"
    elif args.hough:
        segmentator = HoughSegmentator(images, image_paths, params)
        experiment_folder_name += "hough"
    elif args.haar_growable_hough:
        segmentator = HaarGrowableHoughSegmentator(images, image_paths, params)
        experiment_folder_name += "haar_growable_hough"
    elif args.border_growable_hough:
        segmentator = BorderLineGrowableHoughSegmentator(images, image_paths, params)
        experiment_folder_name += "border_growable_hough"
        convex_hull_allow_split = True
    else:
        raise ValueError("Please select a segmentator")

    region_transformer = RegionTransformer(convex_hull_allow_split=convex_hull_allow_split) \
            if params.run_region_growing else None
    if args.write_to_file:
      if not os.path.exists(f'{experiment_folder_name}'):
        os.makedirs(f'{experiment_folder_name}')
      params.save_to_file(experiment_folder_name)

    gt_stats = get_gt_stats(images, image_paths, image_title_gt_image_mapping, gt_images, params)

    processor = Processor(segmentator, images, image_paths, gt_images, image_title_gt_image_mapping, 
                         region_transformer, gt_stats, params,
                         experiment_folder_name if args.write_to_file else None, args.compare_with_file,
                         args.adaptive_thresholding)
    return processor.run()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--raw", help="Convert raw images to png, save them and then load them from the files", action="store_true")
    parser.add_argument("--simple", help="Use simple segmentator", action="store_true")
    parser.add_argument("--wavelet", help="Use wavelet transform to segment", action="store_true")
    parser.add_argument("--hough", help="Use hough segmentator", action="store_true")
    parser.add_argument("--haar_growable_hough", help="Use haar growable hough segmentator", action="store_true")
    parser.add_argument("--border_growable_hough", help="Use border growable hough segmentator", action="store_true")
    parser.add_argument("--jsrt", help="Use JSRT Dataset", action="store_true")
    parser.add_argument("--montgomery", help="Use Montgomery Dataset", action="store_true")
    parser.add_argument("--write-to-file", help="Write scores to file", action="store_true")
    parser.add_argument("--compare-with-file", help="Compare with scores in file")
    parser.add_argument("--experiment-name", help="Experiment name under which to write output")
    parser.add_argument("--adaptive-thresholding", help="Use most probable lung point to select threshold per image",
                        action="store_true")
    args = parser.parse_args()

    if args.write_to_file:
        if not args.experiment_name:
            raise ValueError("An experiment name is required when requested to write to file.")

    jaccard, dices = run_experiment(args, default_params)
    print(f"Jaccard {jaccard}, Dices {dices}")



import cv2 as cv
import seaborn as sns
from collections import defaultdict
from enum import Enum, auto
import matplotlib.pyplot as plt
import numpy as np
import os
from typing import NamedTuple, Any, Sequence, Optional, Mapping

from src.config import VISUALIZATION, VISUALIZATION_FINAL_RESULT, VISUALIZATION_PREPROCESSING, \
    VISUALIZATION_SEGMENTATOR, VISUALIZATION_REGION_GROWING_MPPOINTS_INTENSITY_BASED, \
    VISUALIZATION_REGION_GROWING_CONVEX_HULL, VISUALIZATION_PAPER_IMAGES

plt.rcParams.update({'font.size': 14})

def visualize_image(image, title = ""):
    cv.imshow(title, image)
    cv.waitKey()

def visualize_image_with_contours(image, contours, title = ""):
    drawn_image = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    drawn_image = cv.drawContours(drawn_image, contours, -1, (255, 0, 0), thickness=3)
    cv.imshow(title, image)
    cv.waitKey()

def visualize_two(image1, image2, title = ""):
    plt.subplot(1, 2, 1),plt.imshow(image1, 'gray')
    plt.xticks([]),plt.yticks([])
    plt.subplot(1, 2, 2),plt.imshow(image2, 'gray')
    plt.title(title)
    plt.xticks([]),plt.yticks([])
    plt.show()

def visualize_images(images, rows, cols, title = ""):
    for i, img in enumerate(images, 1):
        plt.subplot(rows, cols, i),plt.imshow(img, 'gray')
        plt.xticks([]),plt.yticks([])
    plt.title(title)
    plt.show()

class EvolvingImage:
    class Stage(Enum):
        ORIGINAL = auto()
        THRESHOLDED = auto()
        STERNUM_AND_LUNG_MP_POINTS = auto()
        SHOULDER_AND_LUNG_MP_POINTS = auto()
        SHOULDER_STERNUM_AND_LUNG_MP_POINTS = auto()
        EDGES = auto()
        INITIAL_CONTOURS = auto()
        LOG_TRANSFORMED = auto()
        CONTRAST_ENHANCED = auto()
        HOUGH_LINES = auto()
        MOST_PROBABLE_LUNG_POINTS = auto()
        MOST_PROBABLE_LUNG_POINTS_ONLY = auto()
        HOUGH_FILTERED_CONTOURS = auto()
        HOUGH_FILTERED_CONTOURS_ON_IMAGE = auto()
        SEGMENTED = auto()
        MP_LUNG_POINTS_SPREAD = auto()
        MP_LUNG_POINTS_AND_EDGES_BLEND = auto()
        CANDIDATE_MP_LUNG_BORDER_POINTS = auto()
        OUTSKIRT_MP_LUNG_BORDER_POINTS = auto()
        CONNECTED_MP_LUNG_BORDER_POINTS = auto()
        LUNG_BORDER_EDGES = auto()
        LUNG_BORDER_CONTOUR = auto()
        GROWN_CONTOURS_DEBUG= auto()
        GROWN_CONTOURS= auto()
        HARRIS_CONTRAST_ENHANCED = auto()
        HARRIS_EDGES = auto()
        HARRIS_SUPPORT_POINTS = auto()
        HARRIS_SEGMENTED = auto()
        HARRIS_LABELED = auto()
        HARRIS_FILTERED_LABELED = auto()
        HARRIS_CONTOURS_ON_IMAGE = auto()
        HARRIS_BEFORE_GROWING_CONTOURS_ON_IMAGE = auto()
        CONVEX_HULL_PROCESS_ILLUSTRATION = auto()
        CONVEX_HULL_SPLIT_LINES = auto()
        CONVEX_HULL_SPLIT_PARTS = auto()
        CONVEX_HULL_SELECTED_CONTOUR = auto()
        FINAL_CONTOURS_ON_IMAGE = auto()
        FINAL_CONTOURS_ON_IMAGE_WITH_GT = auto()
        FINAL_CONTOURS_FILLED = auto()

    class Plot(NamedTuple):
      label: str
      data: Any
      data_x: Any

    class SharedPlot(NamedTuple):
      name: str
      x_label: str
      y_label: str
      plots: Mapping[str, "EvolvingImage.Plot"]
      show_title: bool
      show_legend: bool
      xticks: Optional[np.array] = None
      yticks: Optional[np.array] = None
      xticks_mayor: Optional[np.array] = None
        
    def __init__(self, original_image, is_left_part, img_number, split_line,
                 shoulder_line_left=None, shoulder_line_right=None,
                 is_full_image=False):
      self.image_per_stage = defaultdict(list)
      self.add_image_in_stage(EvolvingImage.Stage.ORIGINAL, original_image)
      self.is_left_part = is_left_part
      self.img_number = img_number
      self.split_line = split_line
      self.shoulder_line_left = shoulder_line_left if shoulder_line_left is not None else []
      self.shoulder_line_right = shoulder_line_right if shoulder_line_right is not None else []
      self.is_full_image = is_full_image
      self.shared_plots = {}

    def __call__(self, stage, idx = 0):
      if stage not in self.image_per_stage:
        if EvolvingImage.Stage.ORIGINAL not in self.image_per_stage:
          raise KeyError(f"Image in stage {stage} was not found")
        else:
          print(f"Image in stage {stage} was not found")
          return np.zeros(self(EvolvingImage.Stage.ORIGINAL).shape, np.uint8)
      return self.image_per_stage[stage][idx][0]

    def add_image_in_stage(self, stage, image=None, elements=None):
      self.image_per_stage[stage].append((image, elements))

    def add_image_in_stage_with_contours(self, stage, image, contours, with_color=False):
      if with_color:
          drawn_image = cv.cvtColor(image, cv.COLOR_GRAY2RGB)
      else:
          drawn_image = image.copy()
      drawn_image = cv.drawContours(drawn_image, contours, -1, (255, 0, 0), thickness=3)
      self.image_per_stage[stage].append((drawn_image, contours))

    def add_shared_plot(self, name, data, labels, xlabel="", ylabel="", show_title=False, show_legend=False,
                        xticks=None, yticks=None, xticks_mayor=None, data_x=None):
      self.shared_plots[name] = EvolvingImage.SharedPlot(name, xlabel, ylabel,
                                                        {label: EvolvingImage.Plot(label, d, data_x) \
                                                             for d, label in zip(data, labels)},
                                                         show_title, show_legend, xticks=xticks, yticks=yticks,
                                                         xticks_mayor=xticks_mayor)

    def get_shared_plot(self, name):
      if name in self.shared_plots:
        return self._shared_plots[name]
      return None

    def get_plot(self, name, label=""):
      shared_plot = self.get_shared_plot(name)
      if shared_plot is not None and  label in shared_plot.plots:
        return shared_plot.plots[label]
      return None

    def elements_in_stage(self, stage, idx=0):
      return self.image_per_stage[stage][idx][1]

    def visualize_all(self):
      if VISUALIZATION_PAPER_IMAGES:
          visualize_images([self(EvolvingImage.Stage.ORIGINAL),
                            self(EvolvingImage.Stage.INITIAL_CONTOURS),
                            self(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS),
                            self(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS)],
                            1, 4, f"INITIAL_CONTOURS, IMG {self.img_number}")
      if VISUALIZATION:
          visualize_image(self(EvolvingImage.Stage.ORIGINAL), "original")
          visualize_two(self(EvolvingImage.Stage.ORIGINAL),
                        self(EvolvingImage.Stage.SEGMENTED))
          visualize_two(self(EvolvingImage.Stage.SEGMENTED),
                        self(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS))

      if VISUALIZATION_PREPROCESSING:
          visualize_images([self(EvolvingImage.Stage.ORIGINAL),
                            self(EvolvingImage.Stage.LOG_TRANSFORMED),
                            self(EvolvingImage.Stage.CONTRAST_ENHANCED)],
                           1, 3, f"Original vs Log transformed vs contrast enhanced, , IMG {self.img_number}")

      if VISUALIZATION_SEGMENTATOR:
          visualize_images([self(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS),
                        self(EvolvingImage.Stage.HARRIS_SEGMENTED),
                        self(EvolvingImage.Stage.HARRIS_SUPPORT_POINTS),
                        self(EvolvingImage.Stage.HARRIS_LABELED),
                        self(EvolvingImage.Stage.HARRIS_FILTERED_LABELED),
                        self(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS_ON_IMAGE),
                        self(EvolvingImage.Stage.HARRIS_CONTOURS_ON_IMAGE)],
                        2, 4, f"Main contour growing, IMG {self.img_number}")

      if VISUALIZATION_REGION_GROWING_MPPOINTS_INTENSITY_BASED:
          visualize_images([self(EvolvingImage.Stage.ORIGINAL), self(EvolvingImage.Stage.MP_LUNG_POINTS_SPREAD),
                            self(EvolvingImage.Stage.MP_LUNG_POINTS_AND_EDGES_BLEND),
                            self(EvolvingImage.Stage.CANDIDATE_MP_LUNG_BORDER_POINTS),
                            self(EvolvingImage.Stage.OUTSKIRT_MP_LUNG_BORDER_POINTS),
                            self(EvolvingImage.Stage.CONNECTED_MP_LUNG_BORDER_POINTS),
                            self(EvolvingImage.Stage.LUNG_BORDER_EDGES),
                            self(EvolvingImage.Stage.LUNG_BORDER_CONTOUR),
                            self(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS),
                            self(EvolvingImage.Stage.GROWN_CONTOURS_DEBUG),
                            self(EvolvingImage.Stage.GROWN_CONTOURS)], 3, 4,
                            f"MP lung points spread, IMG {self.img_number}")

      if VISUALIZATION_REGION_GROWING_CONVEX_HULL:
          visualize_images([self(EvolvingImage.Stage.ORIGINAL),
                            self(EvolvingImage.Stage.CONVEX_HULL_PROCESS_ILLUSTRATION),
                            self(EvolvingImage.Stage.CONVEX_HULL_SPLIT_LINES),
                            self(EvolvingImage.Stage.CONVEX_HULL_SPLIT_PARTS),
                            self(EvolvingImage.Stage.CONVEX_HULL_SELECTED_CONTOUR),
                            self(EvolvingImage.Stage.EDGES)], 2, 3,
                            f"Convexity fixes, IMG {self.img_number}")

      if VISUALIZATION_FINAL_RESULT:
          visualize_image(self(EvolvingImage.Stage.FINAL_CONTOURS_ON_IMAGE_WITH_GT),
                          f"final result with gt, IMG {self.img_number}")

    def save_all(self, experiment_folder_name):
      folder_name = f'{experiment_folder_name}/{self.img_number}'
      if not os.path.exists(f'{folder_name}'):
        os.makedirs(f'{folder_name}')
      if self.is_full_image:
        side_name = "full"
      else:
        side_name = "left" if self.is_left_part else "right"
      for s in EvolvingImage.Stage:
        for i, (im, _) in enumerate(self.image_per_stage[s]):
          cv.imwrite(f"{folder_name}/{s.name}_{i}_{side_name}.png", im)

      sns.set_style('darkgrid')
      for _, sp in self.shared_plots.items():
          fig = plt.figure()
          ax = fig.add_subplot(1, 1, 1)
          plt.xlabel(sp.x_label);
          plt.ylabel(sp.y_label);
          if sp.xticks is not None:
              ax.set_xticks(sp.xticks, minor=True)
          if sp.xticks_mayor is not None:
              ax.set_xticks(sp.xticks_mayor, minor=False)
          if sp.yticks is not None:
              ax.set_yticks(sp.yticks, minor=True)
          ax.grid(which='minor', alpha=0.75)
          if sp.show_title:
              plt.title(sp.name);
          for _, p in sp.plots.items():
              if p.data_x is None:
                  plt.plot(p.data, label=p.label)
              else:
                  plt.plot(p.data_x, p.data, label=p.label)
          if sp.show_legend:
            ax.legend();
          plt.savefig(f"{folder_name}/{sp.name}_{i}_{side_name}.png")
          plt.clf()

    def visualize_multiple_layers(stages):
      final_img = self.image_per_stage[stages[0]][0]

    @staticmethod  
    def visualize_join_image_in_stage(evolving_img_left, evolving_img_right, stage):
      pass


import time
from abc import ABC, abstractmethod
from collections import deque
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from typing import NamedTuple

from src.hough_segmentator import HoughSegmentator
from src.region_transformer import GrowProfile, RegionTransformer
from src.util import sliding_window, kmeans_clustering, get_connected_components_image, \
        closest_contour_point, get_connected_component_points, intensity_stats_on_line, \
        last_non_zero, first_non_zero, iqr_outlier_detector, point_distance, \
        find_convexity_defects, z_score_outlier_detector, modified_z_score_outlier_detector
from src.visualization_utils import EvolvingImage, visualize_two, visualize_images
from src.morphological_transformer import MorphologicalTransformer
from src.region_transformer import RegionTransformer


class ContourWithStats(NamedTuple):
  contour: np.array
  cnt_last_non_zero_for_each_column: np.array
  cnt_last_non_zero_for_each_row: np.array
  cnt_first_non_zero_for_each_row: np.array
  bounding_rect_top_left_x: int
  bounding_rect_top_left_y: int
  bounding_rect_width: int
  bounding_rect_height:int

def get_replacement_segments(contour, cc_points, replace_index1, replace_index2):
  cc_points_as_contour = np.array([[point] for point in cc_points])
  if replace_index1 > replace_index2:
    seg1_start = replace_index1
    seg2_end = replace_index2
  else:
    seg1_start = replace_index2
    seg2_end = replace_index1

  seg1_end = contour.shape[0] - 1
  seg2_start = 0
  min_index_seg1_start, min_dist_seg1_start, closest_point_seg1_start = \
      closest_contour_point((contour[seg1_start][0][0], contour[seg1_start][0][1]),
                             cc_points_as_contour)
  min_index_seg1_end, min_dist_seg1_end, closest_point_seg1_end = \
      closest_contour_point((contour[seg1_end][0][0], contour[seg1_end][0][1]),
                             cc_points_as_contour)
  min_index_seg2_start, min_dist_seg2_start, closest_point_seg2_start = \
      closest_contour_point((contour[seg2_start][0][0], contour[seg2_start][0][1]),
                             cc_points_as_contour)
  min_index_seg2_end, min_dist_seg2_end, closest_point_seg2_end = \
      closest_contour_point((contour[seg2_end][0][0], contour[seg2_end][0][1]),
                             cc_points_as_contour)

  if min_index_seg1_start <= min_index_seg1_end:
    replace_seg1_start_index = min_index_seg1_start
    replace_seg1_end_index = min_index_seg1_end
  else:
    replace_seg1_start_index = min_index_seg1_end
    replace_seg1_end_index = min_index_seg1_start

  cc_points_as_contour_seg1 = cc_points_as_contour[replace_seg1_start_index:replace_seg1_end_index]

  if min_index_seg2_start <= min_index_seg2_end:
    replace_seg2_start_index = min_index_seg2_start
    replace_seg2_end_index = min_index_seg2_end
  else:
    replace_seg2_start_index = min_index_seg2_end
    replace_seg2_end_index = min_index_seg2_start

  cc_points_as_contour_seg2 = cc_points_as_contour[replace_seg2_start_index:replace_seg2_end_index]
  return [(seg1_start, seg1_end, cc_points_as_contour_seg1),
          (seg2_start, seg2_end, cc_points_as_contour_seg2)]


class GrowableHoughSegmentator(HoughSegmentator):
  def __init__(self, images, image_paths, params):
    super().__init__(images, image_paths, params)
    self._region_transformer = RegionTransformer() if params.growable_hough_run_convex_hull else None
    self._after_grow_morphological_transformer = \
        MorphologicalTransformer(1, 1,
                                 5, 1)

  @staticmethod
  def find_replace_indices(contour_size, cc_size, index, max_size_contour_replacement):
    replace_range = min(max_size_contour_replacement, cc_size)
    replace_start_index = max(0, index - int(replace_range / 2))
    replace_end_index = min(contour_size, index + int(replace_range / 2))
    replace_first_range = index - replace_start_index
    replace_second_range = replace_end_index - index
    diff_to_range = replace_range - (replace_first_range + replace_second_range)
    if diff_to_range > 0:
        if replace_start_index == 0:
            replace_end_index += diff_to_range
        elif replace_end_index == contour_size:
            replace_start_index -= diff_to_range
        else:
            assert(diff_to_range == 1)
            replace_start_index -= diff_to_range

    replace_first_range = index - replace_start_index
    replace_second_range = replace_end_index - index
    return replace_start_index, replace_end_index

  @staticmethod
  def cluster_indices(clust_num, labels_array):
    return np.where(labels_array == clust_num)[0]

  @abstractmethod
  def get_support_connected_components(self, evolving_image, filtered_segmented_image, contour_with_stats):
    pass

  @abstractmethod
  def keep_connected_component(self, evolving_image, filtered_segmented_image):
    pass

  def filter_cc_points(self, evolving_image, centroid, cc_points, contour_with_stats, binary_labels_image):
    return cc_points, []

  def filtered_point_image_components(self, filtered_point_image):
    connectivity = self._params.components_connectivity
    connected_components = cv.connectedComponentsWithStats(filtered_point_image, connectivity, cv.CV_32S)
    return connected_components

  def segment_one(self, evolving_image, image_title, **kwargs):
    start_time = time.time()
    filtered_segmented_image, contours_to_keep = super().segment_one(evolving_image, image_title, **kwargs)
    print("--- %s seconds Hough Segment one ---" % (time.time() - start_time))
    start_time = time.time()
    if self._region_transformer:
      grown_contours = []    
      for cnt in contours_to_keep:
          grown_contours.append(
                  self._region_transformer.smoothen_convex_hull(
                      cnt, filtered_segmented_image, evolving_image, self._params))
      print("--- %s seconds region transformer ---" % (time.time() - start_time))
      contours_to_keep = grown_contours.copy()

      filtered_segmented_image = np.zeros(filtered_segmented_image.shape, np.uint8)
      filtered_segmented_image = cv.drawContours(filtered_segmented_image, contours_to_keep, -1, (255, 0, 0),
                                                 thickness=cv.FILLED)

    image = evolving_image(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED).copy()
    evolving_image.add_image_in_stage_with_contours(EvolvingImage.Stage.HARRIS_BEFORE_GROWING_CONTOURS_ON_IMAGE,
                                                    image.copy(), grown_contours)
    h, w = image.shape[:2]
    im_height, im_width = image.shape
    debug_image1 =  cv.cvtColor(image, cv.COLOR_GRAY2RGB)
    binary_labels_image = np.zeros(image.shape, np.uint8)
    support_points_image =  cv.cvtColor(image, cv.COLOR_GRAY2RGB)

    cnt_x, cnt_y, cnt_w, cnt_h = cv.boundingRect(contours_to_keep[0])
    contour_with_stats = ContourWithStats(contour=contours_to_keep[0], 
                                          cnt_last_non_zero_for_each_column=last_non_zero(filtered_segmented_image),
                                          cnt_last_non_zero_for_each_row=last_non_zero(filtered_segmented_image, axis=1),
                                          cnt_first_non_zero_for_each_row=first_non_zero(filtered_segmented_image, axis=1),
                                          bounding_rect_top_left_x=cnt_x,
                                          bounding_rect_top_left_y=cnt_y,
                                          bounding_rect_width=cnt_w,
                                          bounding_rect_height=cnt_h)
    start_time = time.time()
    connected_components = self.get_support_connected_components(evolving_image,
                                                                 filtered_segmented_image,
                                                                 contour_with_stats)
    print("--- %s seconds get support cc ---" % (time.time() - start_time))
    labels = connected_components[1]
    labeled_img, _ = get_connected_components_image(labels)
    stats = connected_components[2]
    centroids = connected_components[3]
    support_points = []
    all_filtered_points = []
    kept_labels = []
    start_time = time.time()
    for i, centroid in enumerate(centroids):
      if i == 0:
          continue
      keep_cc, contour_replacement_point_idx = \
          self.keep_connected_component(image, centroid, labels, stats, i, support_points_image,
                                       contours_to_keep[0], support_points)
      if not keep_cc:
        continue
      kept_labels.append(i)
      cc_points = get_connected_component_points(labels, i)
      for p in cc_points:
        binary_labels_image[p[1], p[0]] = 255
    print("--- %s seconds first filtering ---" % (time.time() - start_time))

    # c = contours_to_keep[0]
    # ext_bot = tuple(c[c[:, :, 1].argmax()][0])
    start_time = time.time()
    for i, centroid in enumerate(centroids):
      if i in kept_labels:
        cc_points = get_connected_component_points(labels, i)
        _, filtered_points = self.filter_cc_points(evolving_image, centroid, cc_points, contour_with_stats,
                                                   binary_labels_image)
        all_filtered_points.extend(filtered_points)
    print("--- %s seconds filter cc points ---" % (time.time() - start_time))

    for fp in all_filtered_points:
      debug_image1[fp[1], fp[0]] = (255, 0, 0)
      binary_labels_image[fp[1], fp[0]] = 0

    filtered_connected_components = self.filtered_point_image_components(binary_labels_image)
    labels = filtered_connected_components[1]
    labeled_img, _ = get_connected_components_image(labels)
    stats = filtered_connected_components[2]
    centroids = filtered_connected_components[3]
    centroid_dists = []
    all_dists = []
    dist_idx_to_label = {}
    for i, c in enumerate(centroids[1:], 1):
      min_index, min_dist, closest_point = closest_contour_point(c, contours_to_keep[0])
      centroid_dists.append(min_dist)
      for dist_idx in range(len(all_dists), len(all_dists) + stats[i, cv.CC_STAT_AREA]):
        dist_idx_to_label[dist_idx] = i
      all_dists.extend([min_dist for _ in range(0, stats[i, cv.CC_STAT_AREA])])

    start_time = time.time()
    kept_labels = []

    modified_contour = np.copy(contours_to_keep[0])
    
    replace_tuples = []
    max_size = min(self._params.max_size_contour_replacement, modified_contour.shape[0])
    for i, centroid in enumerate(centroids):
      if i == 0 or stats[i, cv.CC_STAT_AREA] < self._params.connected_component_min_size:
        continue
      # Outliers found for labels >= 1
      kept_labels.append(i)
      cc_points = get_connected_component_points(labels, i)
      cc_points_as_contour = np.array([[point] for point in cc_points])
      min_index1, min_dist1, closest_point1 = closest_contour_point((cc_points[0][0], cc_points[0][1]), contours_to_keep[0])
      min_index2, min_dist2, closest_point2 = closest_contour_point((cc_points[-1][0], cc_points[-1][1]), contours_to_keep[0])
      cv.circle(labeled_img, (cc_points[0][0], cc_points[0][1]), 5, (255, 255, 0))
      cv.circle(labeled_img, (cc_points[-1][0], cc_points[-1][1]), 5, (0, 255, 255))

      cc_size = cc_points_as_contour.shape[0]
      if min_index1 <= min_index2:
        replace_start_index = min_index1
        replace_end_index = min_index2
      else:
        replace_start_index = min_index2
        replace_end_index = min_index1
        cc_points_as_contour = cc_points_as_contour[::-1]

      cv.circle(labeled_img, (contours_to_keep[0][replace_start_index][0][0], contours_to_keep[0][replace_start_index][0][1]), 4, (255, 255, 0))
      cv.circle(labeled_img, (contours_to_keep[0][replace_end_index][0][0], contours_to_keep[0][replace_end_index][0][1]), 4, (0, 255, 255))

      # Handle edge case: Replacement CC right where contour starts
      min_index, min_dist, closest_point = closest_contour_point(centroid, contours_to_keep[0])

      if (min_index >= replace_end_index or min_index <= replace_start_index) and \
            replace_end_index - replace_start_index > 100:
        replace_tuples.extend(
            get_replacement_segments(contours_to_keep[0], cc_points, replace_start_index, replace_end_index))
        continue

      assert replace_start_index <= replace_end_index
      replace_tuples.append((replace_start_index, replace_end_index, cc_points_as_contour))

    replace_tuples = sorted(replace_tuples, key=lambda x: x[0])
    prev_end_index = None
    # Avoid modifying original contour, but combine at the end
    modified_contours = []
    for replace_start_index, replace_end_index, cc_points_as_contour in replace_tuples:
      if prev_end_index is None or replace_start_index > prev_end_index:
        start = replace_start_index
        end = replace_end_index
        prev_end_index = end
      elif replace_end_index > prev_end_index:
        start = prev_end_index + 1
        end = replace_end_index
        prev_end_index = end
      else:
        assert replace_end_index <= prev_end_index
        continue
      modified_contours.append(np.append(np.append(modified_contour[:start], cc_points_as_contour[:], axis=0),
                               modified_contour[end:], axis=0))


    filter_out_labels = list(set(range(0, filtered_connected_components[0])) - set(kept_labels))
    
    combined_segmented_image =  np.zeros(image.shape, np.uint8)
    cv.drawContours(combined_segmented_image, modified_contours, -1, (255, 0, 0),
                    thickness=cv.FILLED)
    combined_segmented_image = np.maximum(combined_segmented_image, filtered_segmented_image)
    combined_segmented_image = self._morphological_transformer.dilate(combined_segmented_image)
    c_im, combined_contours, hierarchy = cv.findContours(combined_segmented_image, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)


    filtered_labeled_img, filtered_labels = get_connected_components_image(labels, filter_out_labels)

    evolving_image.add_image_in_stage(EvolvingImage.Stage.HARRIS_LABELED, labeled_img)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.HARRIS_FILTERED_LABELED, filtered_labeled_img)
    evolving_image.add_image_in_stage_with_contours(EvolvingImage.Stage.HARRIS_CONTOURS_ON_IMAGE,
                                                    image, combined_contours)
    return combined_segmented_image, combined_contours


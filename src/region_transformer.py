from abc import ABC, abstractmethod
from math import sqrt
from rdp import rdp

import cv2 as cv
import time
from collections import defaultdict, deque
from functools import cmp_to_key
import numpy as np
from src.config import Params
from src.morphological_transformer import MorphologicalTransformer
from src.util import point_distance, nearest_points, get_line, get_angle, line_intersection, \
    find_convexity_defects, BBBox, min_dist_point_to_line, get_line_full
from src.visualization_utils import EvolvingImage, visualize_two
from typing import NamedTuple, Tuple


neighbors = [(-1, 0), (1, 0), (0, -1), (0, 1)]
all_neighbors = [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1)]
BORDER_POINT_UNCERTAINTY_PX = 50
BORDER_AVG_INTESITY_DIFF_THRESHOLD = 50.


class ConvexArea(NamedTuple):
  start: Tuple[int, int]
  end: Tuple[int, int]
  farthest_point: Tuple[int, int]
  contour: np.array

class ContourSplitsStatistics:
    def __init__(self, cnt_image, original_image):
        self._cnt_image = cnt_image
        self._original_image = original_image
        self._is_above = lambda p, a,b: np.cross(p-a, b-a) < 0
        self.draw_img = cv.cvtColor(cnt_image, cv.COLOR_GRAY2RGB)

    def reset_draw_img(self):
        self.draw_img = cv.cvtColor(self._cnt_image, cv.COLOR_GRAY2RGB)

    def _get_points_in_splits(self, start, end, points):
        if points.size == 0:
          return
        start_x = start[0]
        start_y = start[1]
        end_x = end[0]
        end_y = end[1]
        a = np.array([start_x, start_y])
        b = np.array([end_x, end_y])
        point_is_above = self._is_above(points, a, b) 
        for i, value in enumerate(point_is_above):
            if self._cnt_image[points[i, 1], points[i, 0]] == 0:
                continue
            yield points[i], point_is_above[i]

    def _count_points_in_splits(self, start, end, points, color_above, color_below):
        count_above = 0
        count_below = 0
        for point, is_above in self._get_points_in_splits(start, end, points):
            if is_above:
                count_above += 1
                cv.circle(self.draw_img, (point[0], point[1]), 2, color_above)
            else:
                count_below += 1
                cv.circle(self.draw_img, (point[0], point[1]), 2, color_below)
        return count_above, count_below

    def points_in_splits(self, start, end):
        p = np.argwhere(self._cnt_image == 255)
        p = np.flip(p, axis=1)
        return self._get_points_in_splits(start, end, p)

    def points_std_in_splits(self, start, end):
        points_above = []
        points_below = []
        for point, is_above in self.points_in_splits(start, end):
          if is_above:
            points_above.append(point)
          else:
            points_below.append(point)
        return np.std(np.array(points_above)), np.std(np.array(points_below))

    def points_number_in_split_parts(self, start, end, color_above, color_below):
        p = np.argwhere(self._cnt_image == 255)
        p = np.flip(p, axis=1)
        return self._count_points_in_splits(start, end, p, color_above, color_below)

    def mp_points_in_split_parts(self, start, end, mp_points, color_above, color_below):
        p = np.array(mp_points)

        return self._count_points_in_splits(start, end, p, color_above, color_below)

    def mp_points_density_in_split_parts(self, start, end, mp_points, color_above, color_below):
        p = np.array(mp_points)
        count_mp_above, count_mp_below = self.mp_points_in_split_parts(start, end, p, color_above, color_below)
        count_above, count_below = self.points_number_in_split_parts(start, end, color_above, color_below)

        return count_mp_above / count_above if count_above > 0 else 0, \
            count_mp_below / count_below if count_below > 0 else 0

    def intensity_avg_in_split_parts(self, start, end):
        masked = cv.bitwise_and(self._original_image, self._original_image, mask=self._cnt_image)
        p = np.argwhere(masked != 0)
        p = np.flip(p, axis=1)
        count_above = 0
        count_below = 0
        sum_above = sum_below = 0.
        for point, is_above in self._get_points_in_splits(start, end, p):
            if is_above:
                sum_above += self._original_image[point[1], point[0]]
                count_above += 1
                cv.circle(self.draw_img, (point[0], point[1]), 2, (255, 0, 0))
            else:
                sum_below += self._original_image[point[1], point[0]]
                count_below += 1
                cv.circle(self.draw_img, (point[0], point[1]), 2, (0, 255, 0))
        return sum_above / count_above, sum_below / count_below

    def intensity_variance_in_split_parts(self, start, end, color_above, color_below):
        pass

    def line_separation_grade(self, point, line_y):
        height, width = self._original_image.shape
        separation_grade = 1.
        total_count = 0
        count = 0
        pixel_range = 20
        if line_y == -1:
            for  y in range (max(point[1] - pixel_range, 0), min(point[1] + pixel_range, height)):
                total_count += 1
                if self._cnt_image[y, point[0]] == 0:
                    count += 1
        else:
            for x in range(max(point[0] - pixel_range, 0), min(point[0] + pixel_range, width)):
                total_count += 1
                if self._cnt_image[line_y(x), x] == 0:
                    count += 1

        return count / total_count


class ModifyProfile(ABC):
    def __init__(self, evolving_image, original_contour = None):
        self._original_image = evolving_image(EvolvingImage.Stage.ORIGINAL)
        self._original_contour = original_contour if original_contour is not None \
            else evolving_image.elements_in_stage(EvolvingImage.Stage.HOUGH_FILTERED_CONTOURS)[0]
        mask = np.zeros(self._original_image.shape, np.uint8)
        self._original_contour_mask = cv.drawContours(mask, [self._original_contour], -1, (255, 0, 0), thickness=cv.FILLED)
        self._original_region_mean = cv.mean(self._original_image, mask)[0]
        self._debug_img = cv.cvtColor(mask, cv.COLOR_GRAY2RGB)

        #Input image parameters
        self._height, self._width = self._original_image.shape
        self._image_size = self._height * self._width

    @abstractmethod
    def neighbor_belongs(self, row, col, neighbor_row, neighbor_col):
        check_inside = (neighbor_row >= 0) & (neighbor_col >= 0) & (neighbor_row < self._height) & (neighbor_col < self._width) 
        return check_inside


class GrowProfile(ModifyProfile):
    def __init__(self, evolving_image, original_contour = None):
        super().__init__(evolving_image, original_contour)
        self._region_threshold = 135.
        self._pixel_intesity_difference_threshold = 150.
        self._to_region_mean_intensity_difference_threshold = 135.
        self._region_mean = self._original_region_mean
        self._intensity_difference = 0
        seed_points = [(x[0][1], x[0][0], 255) for x in self._original_contour]
        self._seed_points = deque(seed_points)
        self._region_size = len(seed_points)
        self._prev_region_size = self._region_size

    @property
    def grown_contour(self):
      return self._original_contour_mask

    @property
    def new_seed_points(self):
      points = np.argwhere(self._original_contour_mask != 0)
      return np.array([[point[1], point[0]] for point in points])

    @property
    def debug_img(self):
      return self._debug_img

    def contour_growth_continue_criteria(self):
        #Region growing until intensity difference becomes greater than certain threshold
        return (self._intensity_difference < self._region_threshold) and (self._region_size < self._image_size) \
            and self._seed_points

    def neighbor_belongs(self, row, col, neighbor_row, neighbor_col):
        if not super().neighbor_belongs(row, col, neighbor_row, neighbor_col):
          return False
        to_region_diff = abs(self._original_image[neighbor_row, neighbor_col] - self._original_region_mean)
        try:
            to_prev_pixel_diff = abs(self._original_image[neighbor_row, neighbor_col] - self._original_image[row, col])
        except:
            print(row, col, neighbor_row, neighbor_col, self._height, self._width)

        if self.grown_contour[neighbor_row, neighbor_col] != 0:
            self._debug_img[neighbor_row, neighbor_col] = (255, 0, 0)
        if self._original_contour_mask[neighbor_row, neighbor_col] != 0 or \
                to_region_diff > self._to_region_mean_intensity_difference_threshold or \
                to_prev_pixel_diff > self._pixel_intesity_difference_threshold:
            return False  
        return True

    def update(self):
        #Update the seed value
        seed = self._seed_points.popleft()
        self._original_contour_mask[seed[0], seed[1]] = seed[2]
        self._prev_region_size = self._region_size
        self._region_size += 1
        self._region_mean = \
            (self._region_mean * self._prev_region_size + self._original_image[seed[0], seed[1]])/(self._region_size)
        self._intensity_difference = abs(self._region_mean - self._original_region_mean)
        return seed

    def add_seed_point(self, row, col):
        to_region_diff = abs(self._original_image[row, col] - self._original_region_mean)
        weight = to_region_diff * (-255 / self._to_region_mean_intensity_difference_threshold) + 255

        self._seed_points.append((row, col, weight))

class ShrinkageProfile(ModifyProfile):
    def __init__(self, evolving_image, initial_contour, original_contour = None):
        super().__init__(evolving_image, original_contour)
        initial_mask = np.zeros(self._original_image.shape, np.uint8)
        self._initial_contour = initial_contour
        self._initial_contour_mask = cv.drawContours(initial_mask, [initial_contour], -1, (255, 0, 0), thickness=cv.FILLED)
        self._region_threshold = 5.
        self._pixel_intesity_difference_threshold = 100.
        self._to_region_mean_intensity_difference_threshold = 135.
        self._region_mean = cv.mean(self._original_image, initial_mask)[0]
        self._intensity_difference = abs(self._region_mean - self._original_region_mean)
        seed_points = [(x[0][0], x[0][1], 255) for x in initial_contour]
        self._seed_points = deque(seed_points)
        self._region_size = cv.contourArea(initial_contour)
        self._prev_region_size = self._region_size

    @property
    def grown_contour(self):
      return self._initial_contour_mask

    @property
    def new_seed_points(self):
      return np.argwhere(self._initial_contour_mask == 255)

    @property
    def debug_img(self):
      return self._debug_img

    def contour_growth_continue_criteria(self):
        #Region shrinking until intensity difference becomes lesser than certain threshold
        return (self._intensity_difference > self._region_threshold) and self._seed_points

    def neighbor_belongs(self, row, col, neighbor_row, neighbor_col):
        if not super().neighbor_belongs(row, col, neighbor_row, neighbor_col):
            return False
        to_region_diff = abs(self._original_image[row, col] - self._original_region_mean)

        check_original_region_inside = self._original_contour_mask[row, col] != 0
        if self.grown_contour[row, col] != 0:
            self._debug_img[row, col] = (255, 0, 0)
        if check_original_region_inside or self._initial_contour_mask[row, col] == 0 or \
                to_region_diff < self._to_region_mean_intensity_difference_threshold:
            return False  
        return True

    def update(self):
        #Update the seed value
        seed = self._seed_points.popleft()
        self._initial_contour_mask[seed[0], seed[1]] = seed[2]
        self._prev_region_size = self._region_size
        self._region_size -= 1
        self._region_mean = \
            (self._region_mean * self._prev_region_size - self._original_image[seed[0], seed[1]])/(self._region_size)
        self._intensity_difference = abs(self._region_mean - self._original_region_mean)
        return seed

    def add_seed_point(self, row, col):
        self._seed_points.append((row, col, 0))


class RegionTransformer:
  def __init__(self, convex_hull_allow_split = True):
      self._convex_hull_allow_split = convex_hull_allow_split

  def shrink_contour_based_on_intensity(cnt, evolving_image):
      pass

  @staticmethod
  def simplify(contour):
    """ Find multiple intersections (>2) to horizontal or vertical lines.

        Find where this anomaly starts and ends. Normally points two closeby belong
        to an anomaly. Start and simple connect them with a line
    """

  @staticmethod
  def rdp_smoothen(contour, epsilon=8.0):
      seed_points = [[x[0][0], x[0][1]] for x in contour]
      return np.array(rdp(seed_points, epsilon=epsilon))

  @staticmethod
  def find_most_probable_contour_splitting_line(cnt_img, point, point_index, mp_points, original_image,
                                                point_edge, contour, params, defect_bb_box):
        valley_point_th = params.convex_hull_valley_point_th
        angle_thresh = 120
        angle_with_start_end_thresh = 130
        contour_size, _, _ = contour.shape
        angle = get_angle(contour[min(point_index + valley_point_th, contour_size - 1)][0], point, contour[max(point_index - valley_point_th, 0)][0])
        angle_with_start_end = get_angle(point_edge[0], point, point_edge[1])
        if abs(angle) > angle_thresh or abs(angle_with_start_end) > angle_with_start_end_thresh:
            return None, None, None
        
        height, width = cnt_img.shape
        new_cnt_img = cv.cvtColor(cnt_img, cv.COLOR_GRAY2RGB)
        hull_line_start, hull_line_end, hull_line_points = get_line(point_edge[0], point_edge[1])
        hull_line_points = list(hull_line_points)
        valley_start_1, valley_end_1, _ = get_line(contour[min(point_index + valley_point_th, contour_size - 1)][0],
                                                   point)
        valley_start_2, valley_end_2, _ = get_line(contour[max(0, point_index - valley_point_th)][0], point)
        inter_x_1, inter_y_1 = line_intersection((valley_start_1, valley_end_1), (hull_line_start, hull_line_end))
        inter_x_2, inter_y_2 = line_intersection((valley_start_2, valley_end_2), (hull_line_start, hull_line_end))
        if inter_x_1 is None or inter_x_2 is None or inter_y_1 is None or inter_y_2 is None:
            return None, None, None
        cv.circle(new_cnt_img, (contour[point_index][0][0], contour[point_index][0][1]), 5, (0, 128, 128))
        num_points_on_thinest = sqrt(height ** 2 + width ** 2)
        max_ratio_non_contour_points_in_defect = 0
        start_of_thinest = None
        end_of_thinest = None
        thin_contour_lines = []
        all_contour_lines = []
        defect_passing_lines = []

        contour_splits_statistics = ContourSplitsStatistics(cnt_img, original_image)
        for point_in_hull_line in hull_line_points:
            # print("HELLO ", point_in_hull_line[0], int(min(inter_x_1, inter_x_2)), int(max(inter_x_1, inter_x_2)))
            # print("HELLO ", point_in_hull_line[1], int(min(inter_y_1, inter_y_2)), int(max(inter_y_1, inter_y_2)))
            if point_in_hull_line[0] not in range(int(min(inter_x_1, inter_x_2)), int(max(inter_x_1, inter_x_2)) + 1) or \
                    point_in_hull_line[1] not in range(int(min(inter_y_1, inter_y_2)), int(max(inter_y_1, inter_y_2)) + 1):
                continue

            num_points_on_current = 0
            defect_num_line_points = 0
            defect_num_non_contour_points = 0
            start, end, points_in_line = get_line_full(point_in_hull_line, point, width - 1, height - 1)
            for point_in_line in points_in_line:
                if defect_bb_box.upper_left[0] <= point_in_line[0] <= defect_bb_box.bottom_right[0] and \
                    defect_bb_box.upper_left[1] <= point_in_line[1] <= defect_bb_box.bottom_right[1]:
                    defect_num_line_points += 1
                if cnt_img[point_in_line[1], point_in_line[0]] != 0:
                    num_points_on_current += 1
                else:
                    if defect_bb_box.upper_left[0] <= point_in_line[0] <= defect_bb_box.bottom_right[0] and \
                        defect_bb_box.upper_left[1] <= point_in_line[1] <= defect_bb_box.bottom_right[1]:
                        defect_num_non_contour_points += 1
            defect_ratio_non_contour_points = defect_num_non_contour_points / defect_num_line_points
            if num_points_on_current <= 200:
                if defect_ratio_non_contour_points > max_ratio_non_contour_points_in_defect:
                    max_ratio_non_contour_points_in_defect = defect_ratio_non_contour_points
                    start_of_thinest = start
                    end_of_thinest = end
            all_contour_lines.append((start, end))
            if num_points_on_current <= 200:
                thin_contour_lines.append((start, end))
            
        for line in all_contour_lines:
            cv.line(new_cnt_img, line[0], line[1], [255, 255, 255], 1)

        for line in thin_contour_lines:
            cv.line(new_cnt_img, line[0], line[1], [255, 255, 0], 1)

        if start_of_thinest is not None and end_of_thinest is not None:
            cv.line(new_cnt_img, start_of_thinest, end_of_thinest, [0, 255, 0], 3)

        return start_of_thinest, end_of_thinest, new_cnt_img
  
  @staticmethod
  def get_contour_to_fill(original_contour, start_index, end_index):

      return np.array([c_p for c_p in original_contour[start_index:end_index + 1]] + [original_contour[start_index]])

  @staticmethod
  def find_splitting_line_inner_separation_contour(cnt_image):
      
      opposite = cv.bitwise_not(cnt_image)
      _, contours, _ = cv.findContours(cnt_image, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
      filled = np.zeros((cnt_image.shape), np.uint8)
      cv.drawContours(filled, contours, -1, (255,0,0), thickness=cv.FILLED)

      inner_separation_contour_image =  cv.bitwise_and(opposite, opposite, mask = filled)

                                         
  def smoothen_convex_hull(self, contour, cnt_img, evolving_image, params):
      # RegionTransformer.find_splitting_line_inner_separation_contour(cnt_img)
      mp_lung_points_only_image = evolving_image(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS_ONLY)
      external_mp_points_image = cv.bitwise_xor(mp_lung_points_only_image, cnt_img, mask=None)
      external_mp_points_image = cv.bitwise_and(mp_lung_points_only_image, external_mp_points_image)
      
      mp_points = \
          evolving_image.elements_in_stage(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS).coords_most_probably_in_lung
      img = cv.cvtColor(cnt_img, cv.COLOR_GRAY2RGB)
      filled_cnt_img = cnt_img.copy()
      new_cnt_img = cnt_img.copy()
      for mp_point in mp_points:
          cv.circle(img, (mp_point[0], mp_point[1]), 2, (0, 255, 0))
      m_tr = MorphologicalTransformer(5, 1, 1, 1)
      eroded = m_tr.erode(cnt_img)
      _, contours, _ =cv.findContours(eroded, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
      if len(contours) > 1:
        max_cnt_area = 0
        cnt = None
        count_big = 0
        for c in contours:
          c_area = cv.contourArea(c)
          if c_area > params.min_contour_area:
            count_big += 1
            if count_big > 1:
              cnt = contour
              break
          if c_area > max_cnt_area:
            cnt = c
            max_cnt_area = c_area
      else:
        assert len(contours) == 1
        cnt = contours[0]
      to_fill_contours = []
      valley_points = []
      split_points = []
      split_point_indices = []
      split_point_edges = []
      border_line_convex_areas = []
      idiomorphy_points = []

      for s, e, f, d in find_convexity_defects(cnt):
          start = tuple(cnt[s][0])
          end = tuple(cnt[e][0])
          far = tuple(cnt[f][0])
          valley_points.append(far)
          cv.line(img, start, end, [0, 255, 0], 2)
          cv.circle(img, far, 5, [255, 255, 0], -1)
          dist_start_to_end = point_distance(start, end)
          dist_to_far = max(point_distance(start, far), point_distance(end, far))
          if dist_start_to_end <= params.convex_hull_unconstrained_fill_max_distance_start_to_end and \
              dist_to_far <= params.convex_hull_unconstrained_fill_max_distance_to_farthest:
                tf_cnt = RegionTransformer.get_contour_to_fill(cnt, s, e)
                to_fill_contours.append(tf_cnt)
          elif dist_start_to_end <= params.convex_hull_mp_points_fill_max_distance_start_to_end and \
              dist_to_far <= params.convex_hull_mp_points_fill_max_distance_to_farthest:
              min_x = min([start[0], end[0], far[0]])
              max_x = max([start[0], end[0], far[0]])
              min_y = min([start[1], end[1], far[1]])
              max_y = max([start[1], end[1], far[1]])
              bb_img = external_mp_points_image[min_y : max_y, min_x : max_x]
              bb_img_area = (max_y - min_y) * (max_x - min_x)
              density = np.sum(bb_img == 255) / bb_img_area
              if density > params.convex_hull_mp_points_fill_min_density:
                tf_cnt = RegionTransformer.get_contour_to_fill(cnt, s, e)
                to_fill_contours.append(tf_cnt)
          elif dist_start_to_end >= params.convex_hull_actual_border_line_distance_start_to_end and \
              dist_to_far <= params.convex_hull_actual_border_line_max_distance_to_farthest:
                border_line_convex_areas.append(ConvexArea(start, end, far,
                  np.array([c_p for c_p in cnt[s:e + 1]] + [cnt[s]])))

      cv.drawContours(img, to_fill_contours, -1, color=(255, 0, 0), thickness=cv.FILLED)
      evolving_image.add_image_in_stage(EvolvingImage.Stage.CONVEX_HULL_PROCESS_ILLUSTRATION, img,
                                        border_line_convex_areas)

      cv.drawContours(filled_cnt_img, to_fill_contours, -1, color=(255, 255, 255), thickness=cv.FILLED)

      new_t, new_contours, hierarchy = cv.findContours(filled_cnt_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
      assert(len(new_contours) == 1)
      filled_cnt = new_contours[0]

      defect_bb_boxes = []
      for s, e, f, d in find_convexity_defects(filled_cnt):
          start = tuple(filled_cnt[s][0])
          end = tuple(filled_cnt[e][0])
          far = tuple(filled_cnt[f][0])
          valley_points.append(far)
          cv.line(img, start, end, [0, 255, 0], 2)
          cv.circle(img, far, 5, [255, 255, 0], -1)
          dist_to_far = max(point_distance(start, far), point_distance(end, far))
          min_dist = min_dist_point_to_line(np.asarray(start), np.asarray(end), np.asarray(far))
          if self._convex_hull_allow_split and dist_to_far > params.convex_hull_separation_min_distance_to_farthest and \
              min_dist > params.convex_hull_separation_min_distance_farthest_to_line:
              min_x = min([start[0], end[0], far[0]])
              max_x = max([start[0], end[0], far[0]])
              min_y = min([start[1], end[1], far[1]])
              max_y = max([start[1], end[1], far[1]])
              defect_bb_boxes.append(BBBox((min_x, min_y), (max_x, max_y)))

              idiomorphy_points += [filled_cnt[ip][0] for ip in range(s, f + 1)]
              split_points.append(far)
              split_point_indices.append(f)
              split_point_edges.append((start, end))
              cv.circle(img, far, 5, [0, 0, 255])
      
      if split_points:
          contour_splits_statistics = ContourSplitsStatistics(filled_cnt_img, evolving_image(EvolvingImage.Stage.ORIGINAL))
          for sp_point, sp_point_index, sp_point_edge, defect_bb_box in \
              zip(split_points, split_point_indices, split_point_edges, defect_bb_boxes):
              start, end, split_lines_img = \
                      RegionTransformer.find_most_probable_contour_splitting_line(filled_cnt_img, sp_point, sp_point_index,
                                                                                  mp_points,
                                                                                  evolving_image(
                                                                                      EvolvingImage.Stage.ORIGINAL),
                                                                                  sp_point_edge, filled_cnt, params, defect_bb_box)
              if start is not None and end is not None:
                  evolving_image.add_image_in_stage(EvolvingImage.Stage.CONVEX_HULL_SPLIT_LINES, split_lines_img)
                  density_above, density_below = contour_splits_statistics.mp_points_density_in_split_parts(start, end,
                      mp_points, (255, 0, 0), (0, 0, 255))
                  points_above, points_below = contour_splits_statistics.points_number_in_split_parts(start, end,
                          (255, 0, 0), (0, 0, 255))
                  mp_points_above, mp_points_below = contour_splits_statistics.mp_points_in_split_parts(start, end,
                          mp_points, (255, 0, 0), (0, 0, 255))
                  idiomorphy_points_above, idiomorphy_points_below = contour_splits_statistics.mp_points_in_split_parts(start, end,
                          idiomorphy_points, (255, 0, 0), (0, 0, 255))
                  evolving_image.add_image_in_stage(EvolvingImage.Stage.CONVEX_HULL_SPLIT_PARTS,
                                                    contour_splits_statistics.draw_img)
                  std_points_above, std_points_below = contour_splits_statistics.points_std_in_splits(start, end)

                  smallest_contour_size = min(points_above, points_below)
                  biggest_contour_size = min(points_above, points_below)
                  eliminate_above = eliminate_below = False
                  if points_above >= points_below and points_above > params.min_contour_area and mp_points_above > 0:
                      if density_below == 0. or density_above / density_below >= params.convex_hull_split_selection_density_ratio:
                          eliminate_below = True
                  elif points_above < points_below and points_below > params.min_contour_area and mp_points_below > 0:
                      if density_above == 0. or density_below / density_above >= params.convex_hull_split_selection_density_ratio:
                          eliminate_above = True
                  else:
                      if density_above >= density_below and points_above > params.min_contour_area:
                          eliminate_below = True
                      if density_above < density_below and points_below > params.min_contour_area:
                          eliminate_above = True

                  if not eliminate_above and not eliminate_below:
                      sum_x_point_above = sum_x_point_below = 0
                      sum_y_point_above = sum_y_point_below = 0
                      count_above = count_below = 0
                      for point, is_above in contour_splits_statistics.points_in_splits(start, end):
                        if is_above:
                            sum_x_point_above += point[0]
                            sum_y_point_above += point[1]
                            count_above += 1
                        else:
                            sum_x_point_below += point[0]
                            sum_y_point_below += point[1]
                            count_below += 1
                      if evolving_image.is_left_part:
                          above_confidence = (sum_x_point_above / count_above - sum_x_point_below / count_below) * 0.5 + \
                              (sum_y_point_above / count_above - sum_y_point_below / count_below) * 0.5
                      else:
                          above_confidence = (sum_x_point_below / count_below - sum_x_point_above / count_above) * 0.5 + \
                              (sum_y_point_above / count_above - sum_y_point_below / count_below) * 0.5
                      if above_confidence > 0:
                          eliminate_below = True
                      else:
                          eliminate_above = True
                  assert not (eliminate_above and eliminate_below)
                  for point, is_above in contour_splits_statistics.points_in_splits(start, end):
                      if (eliminate_above and is_above) or (eliminate_below and not is_above):
                         new_cnt_img[point[1], point[0]] = 0

      cv.drawContours(new_cnt_img, to_fill_contours, -1, color=(255, 255, 255), thickness=cv.FILLED)
      new_t, new_contours, hierarchy = cv.findContours(new_cnt_img, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
      evolving_image.add_image_in_stage(EvolvingImage.Stage.CONVEX_HULL_SELECTED_CONTOUR, new_cnt_img)
      if not new_contours:
          print("Smoothening not possible")
          return contour

      new_cnt_img_all =  np.zeros(new_cnt_img.shape, np.uint8)
      cv.drawContours(new_cnt_img_all, new_contours, -1, color=(255, 255, 255), thickness=cv.FILLED)
      return max(new_contours, key=cv.contourArea)

  @staticmethod
  def smoothen(contour):
      epsilon = 0.1 * cv.arcLength(contour, True)
      approx = cv.approxPolyDP(contour, epsilon, True)
      return approx

  @staticmethod
  def combinatory_grow(evolving_image, contour, grow = True):
      modify_profile = GrowProfile(evolving_image) if grow else ShrinkageProfile(evolving_image, contours[0])
      new_seed_points, intensity_cost_image = RegionTransformer.grow(evolving_image, modify_profile)
      return new_seed_points

  @staticmethod
  def most_probable_lung_points_cost(evolving_image):
      class PointWithLevel(NamedTuple):
          coords: Tuple[float, float]
          level: int

      def cmp_coords(point1, point2):
        if point1[0] > point2[0]:
          return 1
        elif point1[0] < point2[0]:
          return -1

        if point1[1] > point2[1]:
          return 1
        elif point1[1] < point2[1]:
          return -1
        else:
          return 0

      def keep_outskirt(point_image):
        kept_outskirt_image =  np.zeros(point_image.shape, np.uint8)
        kept_outskirt_points_left = []
        kept_outskirt_points_right = []
        kept_outskirt_points_top = []
        kept_outskirt_points_bottom = []
        for x, row in enumerate(point_image):
            white_pixels = np.argwhere(row != 0)
            if white_pixels.size != 0:
                kept_outskirt_image[x, white_pixels[0][0]] = point_image[x, white_pixels[0][0]]
                kept_outskirt_image[x, white_pixels[-1][0]] = point_image[x, white_pixels[-1][0]]
                kept_outskirt_points_left.append((x, white_pixels[0][0]))
                kept_outskirt_points_right.append((x, white_pixels[-1][0]))

        for x, row in enumerate(cv.transpose(point_image)):
            white_pixels = np.argwhere(row != 0)
            if white_pixels.size != 0:
                kept_outskirt_image[white_pixels[0][0], x] = point_image[white_pixels[0][0], x]
                kept_outskirt_image[white_pixels[-1][0], x] = point_image[white_pixels[-1][0], x]
                kept_outskirt_points_top.append((white_pixels[0][0], x))
                kept_outskirt_points_bottom.append((white_pixels[-1][0], x))
        return kept_outskirt_image, kept_outskirt_points_left, kept_outskirt_points_right,\
               kept_outskirt_points_top, kept_outskirt_points_bottom


      def get_contour_intersecting_points(edges_image, points_left, points_right, points_top,
                                          points_bottom, morphology_transformer = None):

          border_point_area_image = np.zeros(edges_image.shape, np.uint8)
          connected_points = np.zeros(edges_image.shape, np.uint8)

          def connect_points_and_draw_confidence_circles(points):
              prev_point = None
              for point in points:
                  cv.circle(border_point_area_image, (point[1], point[0]), 15, (255, 255, 255), thickness=-1,
                            lineType=8, shift=0)
                  # TODO ordering of points must be dictated internally!
                  if prev_point:
                      cv.line(connected_points, (point[1], point[0]), (prev_point[1], prev_point[0]), (255, 255, 255), thickness=3, lineType=8)
                  prev_point = point

          connect_points_and_draw_confidence_circles(points_top)
          connect_points_and_draw_confidence_circles(points_bottom)
          connect_points_and_draw_confidence_circles(points_left)
          connect_points_and_draw_confidence_circles(points_right)
          evolving_image.add_image_in_stage(EvolvingImage.Stage.CONNECTED_MP_LUNG_BORDER_POINTS, connected_points)
          border_edges = cv.bitwise_and(edges_image, border_point_area_image)
          border_edges = cv.bitwise_or(border_edges, connected_points)
          border_edges, _, _, _, _ = keep_outskirt(border_edges)
          m_tr = MorphologicalTransformer(2, 1, 5, 3)
          border_edges = m_tr.dilate(border_edges)
          evolving_image.add_image_in_stage(EvolvingImage.Stage.LUNG_BORDER_EDGES, border_edges)
          im2, contours, hierarchy = cv.findContours(border_edges, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
          evolving_image.add_image_in_stage_with_contours(EvolvingImage.Stage.LUNG_BORDER_CONTOUR,
              evolving_image(EvolvingImage.Stage.ORIGINAL), contours, with_color=True)

          return contours

      mp_points = \
          evolving_image.elements_in_stage(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS).coords_most_probably_in_lung
      image = evolving_image(EvolvingImage.Stage.MOST_PROBABLE_LUNG_POINTS)
      height, width, _ = image.shape
      # pixels with bigger value have smaller cost to be added in the region
      i_cost_image =  np.zeros(image.shape, np.float)

      # Use a linear descending function to calculate contribution of each lung point, only in vertical direction
      # The contribution to the first level of neighbors (immediately top / bottom pixels) is the maximum, aka 1.0
      # Consider that after 12 levels of neighbors contribution is zero. From the linear equation y = ax + b we get
      # a = -0.091, b = 1.091 so y = -0.091x + 1.091
      start_time = time.time()
      for mp_point in mp_points:
          i_cost_image[mp_point[1], mp_point[0]] += 1.0
          for level in range(1, 13):
              neighbor_val = -0.091 * level + 1.091
              if mp_point[1] + level < height:
                  i_cost_image[mp_point[1] + level, mp_point[0]] += neighbor_val
              if mp_point[1] - level >= 0:
                  i_cost_image[mp_point[1] - level, mp_point[0]] += neighbor_val
      i_cost_image *= 255. / i_cost_image.max()
      i_cost_image = i_cost_image.astype(np.uint8)
      evolving_image.add_image_in_stage(EvolvingImage.Stage.MP_LUNG_POINTS_SPREAD, i_cost_image)
      print("--- %s seconds ---" % (time.time() - start_time))
      edges = evolving_image(EvolvingImage.Stage.EDGES).astype(np.uint8)
      blend = cv.bitwise_and(i_cost_image, i_cost_image, mask=edges)
      evolving_image.add_image_in_stage(EvolvingImage.Stage.MP_LUNG_POINTS_AND_EDGES_BLEND, blend)
      indices = np.nonzero(i_cost_image)
      original_image = evolving_image(EvolvingImage.Stage.ORIGINAL)
      candidate_border_point_image =  np.zeros(image.shape, np.uint8)
      start_time = time.time()
      for x, y in zip(indices[0], indices[1]):
          sum_top = sum_bottom = sum_left = sum_right = 0.
          count_top = count_bottom = count_left = count_right = 0
          for y_changing in range(max(0, y - BORDER_POINT_UNCERTAINTY_PX), y):
            sum_top += original_image[x, y_changing]
            count_top += 1

          for y_changing in range(y + 1, min(width - 1, y + BORDER_POINT_UNCERTAINTY_PX + 1)):
            sum_bottom += original_image[x, y_changing]
            count_bottom += 1

          for x_changing in range(max(0, x - BORDER_POINT_UNCERTAINTY_PX), x):
            sum_left += original_image[x_changing, y]
            count_left += 1

          for x_changing in range(x + 1, min(height - 1, x + BORDER_POINT_UNCERTAINTY_PX + 1)):
            sum_right += original_image[x_changing, y]
            count_right += 1

          if (count_top > 0 and count_bottom > 0 and
              abs(sum_top / count_top - sum_bottom / count_bottom) > BORDER_AVG_INTESITY_DIFF_THRESHOLD) or \
                  (count_left > 0 and count_right > 0 and
                   abs(sum_left / count_left - sum_right / count_right) > BORDER_AVG_INTESITY_DIFF_THRESHOLD):
              candidate_border_point_image[x, y] = i_cost_image[x, y]
      print("--- %s seconds 2 ---" % (time.time() - start_time))
      evolving_image.add_image_in_stage(EvolvingImage.Stage.CANDIDATE_MP_LUNG_BORDER_POINTS, candidate_border_point_image)
      start_time = time.time()
      keep_outskirt_image, kept_outskirt_points_left, kept_outskirt_points_right, kept_outskirt_points_top, kept_outskirt_points_bottom =\
          keep_outskirt(candidate_border_point_image)
      evolving_image.add_image_in_stage(EvolvingImage.Stage.OUTSKIRT_MP_LUNG_BORDER_POINTS, keep_outskirt_image)
      print("--- %s seconds 3 ---" % (time.time() - start_time))
      m_tr = MorphologicalTransformer(2, 1, 2, 1)
      edges_new = m_tr.dilate(evolving_image(EvolvingImage.Stage.EDGES))
      contours = get_contour_intersecting_points(edges_new, kept_outskirt_points_left, kept_outskirt_points_right,
                                                 kept_outskirt_points_top, kept_outskirt_points_bottom)
      return cv.drawContours(np.zeros(edges.shape, np.uint8), contours, -1, (255, 0, 0), thickness=-1)

  @staticmethod
  def grow(evolving_image, grow_profile):
    while grow_profile.contour_growth_continue_criteria():
        seed = grow_profile.update()
        #Loop through neighbor pixels
        for i in range(8):
            #Compute the neighbor pixel position
            row_new = seed[0] + all_neighbors[i][0]
            col_new = seed[1] + all_neighbors[i][1]
            #Boundary Condition - check if the coordinates are inside the image
            if not grow_profile.neighbor_belongs(seed[0], seed[1], row_new, col_new):
              continue

            grow_profile.add_seed_point(row_new, col_new)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.GROWN_CONTOURS, grow_profile.grown_contour)
    evolving_image.add_image_in_stage(EvolvingImage.Stage.GROWN_CONTOURS_DEBUG, grow_profile.debug_img)
    return np.array(grow_profile.new_seed_points), grow_profile.grown_contour

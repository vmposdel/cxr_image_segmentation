import abc

class BaseSegmentator:
  __metaclass__ = abc.ABCMeta

  @abc.abstractmethod
  def segment(self):
    """Segment all images"""
    return

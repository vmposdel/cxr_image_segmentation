from typing import NamedTuple

from symmetry.detect import superm2

class SymmetryDetector:
  @staticmethod
  def run_for_image(image):
    symmetry_line = superm2(image)
    return symmetry_line

  @staticmethod
  def cut_triangle_up_to_pixel(image, symmetry_line_start_x, symmetry_line_slope, triangle_end_x, triangle_end_y,
                               new_intensity_value):
      class LineEquation(NamedTuple):
        slope: float
        point_x: int
        point_y: int

        def __call__(self, x):
          return self.slope * (x - self.point_x) + self.point_y

      def get_line():
        #TODO take in account the slope

        line_slope = (triangle_end_y - 0) / (triangle_end_x - symmetry_line_start_x)

        return LineEquation(slope=line_slope, point_x= symmetry_line_start_x, point_y=0)
      if triangle_end_x == symmetry_line_start_x:
        return image

      line = get_line()

      start_x = min(symmetry_line_start_x, triangle_end_x)
      end_x = max(symmetry_line_start_x, triangle_end_x)

      line_coords = []
      for x in range(start_x, end_x + 1):
        for y in range(0, int(line(x))):
          image[y,x] = new_intensity_value
          line_coords.append((y, x))

      return image, line_coords      


      

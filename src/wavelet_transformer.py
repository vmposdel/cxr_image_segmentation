import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np

import pywt

from src.base_segmentator import BaseSegmentator

class WaveletTransformer(BaseSegmentator):
  def __init__(self, images, image_paths):
    self._images = images
    self._image_paths = image_paths

  def segment(self):
    return self.compute_discrete_wavelet_transform()

  def compute_discrete_wavelet_transform(self):
    for image, image_title in zip(self._images, self._image_paths):
      # Load image
      original = image
      
      # Wavelet transform of image, and plot approximation and details
      titles = ['Approximation ' + image_title, ' Horizontal detail ' + image_title,
                'Vertical detail ' + image_title, 'Diagonal detail ' + image_title]
      coeffs2 = pywt.dwt2(original, 'bior1.3')
      LL, (LH, HL, HH) = coeffs2
      fig = plt.figure(figsize=(12, 3))
      for i, a in enumerate([LL, LH, HL, HH]):
          ax = fig.add_subplot(1, 4, i + 1)
          ax.imshow(a, interpolation="nearest", cmap=plt.cm.gray)
          ax.set_title(titles[i], fontsize=10)
          ax.set_xticks([])
          ax.set_yticks([])
      
      fig.tight_layout()
      plt.show()
      yield HH

  def compute_multilevel_discrete_wavelet_transform(self):
    for image, image_title in zip(self._images, self._image_paths):
      mode = 'haar'
      coeffs=pywt.wavedec2(image, mode, level=10)

      #Process Coefficients
      coeffs_H=list(coeffs)  
      coeffs_H[0] *= 0;  

      # reconstruction
      imArray_H=pywt.waverec2(coeffs_H, mode);
      imArray_H *= 255;
      imArray_H =  np.uint8(imArray_H)
      #Display result
      plt.subplot(1, 2, 1),plt.imshow(image, 'gray')
      plt.xticks([]),plt.yticks([])

      plt.subplot(1, 2, 2),plt.imshow(imArray_H, 'gray')
      plt.title(image_title)
      plt.xticks([]),plt.yticks([])
      plt.show()
      yield imArray_H


import csv
import cv2 as cv
from matplotlib import pyplot as plt
from sklearn.metrics import jaccard_similarity_score
import math
import numpy as np
from src.config import VISUALIZATION_EVALUATOR

class ResultGroundtruthImageMapper:
    def __init__(self):
        self._result_image_path_mapping = {}
        self._groundtruth_image_path_mapping = {}
    
    def get_result_for_groundtruth(self, gt):
        return self._result_image_path_mapping.get(gt, None)

    def get_ground_truth_for_result(self, result):
        return self._groundtruth_image_path_mapping.get(result, None)

    def add_result_image(self, image_path, image):
        pass

    def add_groundtruth_image(self, image_path, image):
        pass

class EvaluationImage:
    def __init__(self,
                 left_part_image = None, right_part_image = None,
                 left_part_contours = None, right_part_contours = None,
                 left_evolving_image = None, right_evolving_image = None):
        self._right_part_path = None
        self._left_part_path = None
        self._right_part_image = right_part_image
        self._left_part_image = left_part_image
        self.right_part_contours = right_part_contours
        self.left_part_contours = left_part_contours
        self.image_height = None
        self.image_width = None
        self.left_evolving_image = left_evolving_image
        self.right_evolving_image = right_evolving_image

    @property
    def right_part(self):
        return self._right_part_image

    @property
    def normalized_right_part(self):
        return cv.normalize(self._right_part_image, None, alpha=0, beta=1, norm_type=cv.NORM_MINMAX, dtype=cv.CV_32F)

    @property
    def left_part(self):
        return self._left_part_image

    @property
    def normalized_left_part(self):
        return cv.normalize(self._left_part_image, None, alpha=0, beta=1, norm_type=cv.NORM_MINMAX, dtype=cv.CV_32F)

class SegmentationEvaluator:
  def __init__(self, gt_images, segmented_images, image_paths, mapping_gt_result):
    self._gt_images = gt_images
    self._segmented_images = segmented_images
    self._image_paths = image_paths
    self._mapping_gt_result = mapping_gt_result

  def calc_classifications(self):
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    for image, gt_image, image_title in zip(self._segmented_images, self._gt_images, self._image_paths):
      for i in range(image.height):
        for j in range(image.width):
          if gt_image[i, j] == 0:
            if image[i, j] == 1:
              fp += 1
            else:
              tn += 1
          else:
            if image[i, j] == 1:
              tp += 1
            else:
              fn += 1
      yield tp, tn, fp, fn

  def _jaccard_similarity_coefficient_one(self, image, gt_image):
    TP = np.sum(image.normalized_right_part[gt_image.normalized_right_part==1]) + \
        np.sum(image.normalized_left_part[gt_image.normalized_left_part==1])
    FP = np.sum(image.normalized_right_part[gt_image.normalized_right_part==0]) + \
        np.sum(image.normalized_left_part[gt_image.normalized_left_part==0])
    FN = np.sum(gt_image.normalized_right_part[image.normalized_right_part==0]) + \
        np.sum(gt_image.normalized_left_part[image.normalized_left_part==0])
    return TP / (FP + TP + FN)

  def _dices_coefficient_one(self, image, gt_image):
    TP = np.sum(image.normalized_right_part[gt_image.normalized_right_part==1]) + \
        np.sum(image.normalized_left_part[gt_image.normalized_left_part==1])
    sum_image = np.sum(image.normalized_right_part) + np.sum(image.normalized_left_part)
    sum_gt_image = np.sum(gt_image.normalized_right_part) + np.sum(gt_image.normalized_left_part)
    return TP * 2.0 / (sum_image + sum_gt_image)

  def _average_contour_distance_one(self, image, gt_image):
      def avg_min_distance_contour_to_contour(contour1, contour2):
          sum_min_dist = 0
          for point1 in contour1:
            min_dist = min([math.sqrt((point2[0][0] - point1[0][0]) ** 2 + (point2[0][1] - point1[0][1]) ** 2)
                            for point2 in contour2])
            sum_min_dist += min_dist
          return sum_min_dist / len(contour1)

      sum_seg_to_gt = 0.
      count_seg_to_gt = 0
      for contour in image.right_part_contours:
          for gt_contour in gt_image.right_part_contours:
              sum_seg_to_gt += avg_min_distance_contour_to_contour(contour, gt_contour)
              count_seg_to_gt += 1

      for contour in image.left_part_contours:
          for gt_contour in gt_image.left_part_contours:
              sum_seg_to_gt += avg_min_distance_contour_to_contour(contour, gt_contour)
              count_seg_to_gt += 1

      sum_gt_to_seg = 0.
      count_gt_to_seg = 0
      for gt_contour in gt_image.right_part_contours:
          for contour in image.right_part_contours:
              sum_gt_to_seg += avg_min_distance_contour_to_contour(gt_contour, contour)
              count_gt_to_seg += 1

      for gt_contour in gt_image.left_part_contours:
          for contour in image.left_part_contours:
              sum_gt_to_seg += avg_min_distance_contour_to_contour(gt_contour, contour)
              count_gt_to_seg += 1
      if count_seg_to_gt == 0 or count_gt_to_seg == 0:
          return None
      avg_cnt_dist = ((sum_seg_to_gt / count_seg_to_gt) + (sum_gt_to_seg / count_gt_to_seg)) / 2
      return avg_cnt_dist

  def write_scores_to_file(self, filename):
    with open(filename, 'w') as f:
      w = csv.writer(f)
      w.writerow(('gt_image_index', 'jaccard', 'dices', 'acd'))
      for gt_image_index, segmented_image_index in self._mapping_gt_result.items():
        image = self._segmented_images[segmented_image_index]
        gt_image = self._gt_images[gt_image_index]
        iou = self._jaccard_similarity_coefficient_one(image, gt_image)
        dice = self._dices_coefficient_one(image, gt_image)
        acd = self._average_contour_distance_one(image, gt_image)
        w.writerow((gt_image_index, iou, dice, acd))

  def compare_with_scores_in_file(self, filename):
    file_scores = {}
    with open(filename, 'r') as f:
      r = csv.DictReader(f)
      for row in r:
        file_scores[int(row['gt_image_index'])] = float(row['jaccard'])

    for gt_image_index, segmented_image_index in self._mapping_gt_result.items():
      image = self._segmented_images[segmented_image_index]
      gt_image = self._gt_images[gt_image_index]
      iou = self._jaccard_similarity_coefficient_one(image, gt_image)
      dice = self._dices_coefficient_one(image, gt_image)
      if file_scores[gt_image_index] > iou:
        print(f"New jaccard: {iou}, previous jaccard {file_scores[gt_image_index]}")

  def jaccard_similarity_coefficient(self, calc_manually):
    """ In pixel level it is given as TP / (FP + TP + FN) """
    count_dropped = 0
    if calc_manually:
      sum_result = 0
      for gt_image_index, segmented_image_index in self._mapping_gt_result.items():
        image = self._segmented_images[segmented_image_index]
        gt_image = self._gt_images[gt_image_index]
        if VISUALIZATION_EVALUATOR:
            plt.subplot(2, 2, 1),plt.imshow(image.left_part, 'gray')
            plt.xticks([]),plt.yticks([])
            plt.subplot(2, 2, 2),plt.imshow(gt_image.left_part, 'gray')
            plt.subplot(2, 2, 3),plt.imshow(image.right_part, 'gray')
            plt.subplot(2, 2, 4),plt.imshow(gt_image.right_part, 'gray')
            plt.xticks([]),plt.yticks([])
            plt.show()

        iou = self._jaccard_similarity_coefficient_one(image, gt_image)
        sum_result += iou
      print("Dropped ", count_dropped)
      return sum_result / (len(self._mapping_gt_result.keys()) - count_dropped)
    else:
      for image, gt_image, image_title in zip(self._segmented_images, self._gt_images, self._image_paths):
        iou = jaccard_similarity_score(gt_image.flatten(), image.flatten())

  def dices_coefficient(self):
    """ 2TP / (2TP + FN + FP)"""

    sum_result = 0
    for gt_image_index, segmented_image_index in self._mapping_gt_result.items():
      image = self._segmented_images[segmented_image_index]
      gt_image = self._gt_images[gt_image_index]
      dice = self._dices_coefficient_one(image, gt_image)
      sum_result += dice
    return sum_result / len(self._mapping_gt_result.keys())

  def average_contour_distance(self):
    """ Average distance between segmentation and GT boundary.
    
    Find minimum distance of each point in segmentation contour to the GT contour. Find average.
    Then find the minimum distance of each point in GT contour to the segmentation contour. Average.
    Sum the two averages and divide by 2.
    """

    sum_result = 0
    for gt_image_index, segmented_image_index in self._mapping_gt_result.items():
      image = self._segmented_images[segmented_image_index]
      gt_image = self._gt_images[gt_image_index]
      avg_cnt_dist = self._average_contour_distance_one(image, gt_image)
      sum_result += avg_cnt_dist
    return sum_result / len(self._mapping_gt_result.keys())

  def costophrenic_angle_overlap(self):
    for image, image_title in zip(self._segmented_images, self._image_paths):
      pass

  def apical_region_overlap(self):
    for image, image_title in zip(self._segmented_images, self._image_paths):
      pass


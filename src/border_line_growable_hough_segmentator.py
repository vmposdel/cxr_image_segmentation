from collections import deque
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

from src.growable_hough_segmentator import GrowableHoughSegmentator
from src.region_transformer import GrowProfile, RegionTransformer
from src.util import sliding_window, kmeans_clustering, get_connected_components_image, \
        closest_contour_point, get_connected_component_points, intensity_stats_on_line, \
        intensity_stats_around_point_in_contour, get_line, last_non_zero, \
        bb_around_point
from src.visualization_utils import EvolvingImage, visualize_two, visualize_images
from src.morphological_transformer import MorphologicalTransformer

class BorderLineGrowableHoughSegmentator(GrowableHoughSegmentator):

  def __init__(self, images, image_paths, params):
    super().__init__(images, image_paths, params)
    self._morphological_transformer = \
        MorphologicalTransformer(params.border_line_erosion_kernel_size,
                                 params.border_line_erosion_iterations, 
                                 params.border_line_dilation_kernel_size,
                                 params.border_line_dilation_iterations)

  def filtered_point_image_components(self, filtered_point_image):
    tr_image = filtered_point_image
    return super().filtered_point_image_components(tr_image)

  def get_support_connected_components(self, evolving_image, filtered_segmented_image, contour_with_stats):
    image = evolving_image(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED).copy()
    im_height, im_width = image.shape
    mp_lung_points_only_image =  np.zeros(image.shape, np.uint8)
    is_left_part = evolving_image.is_left_part
    for x, y, bb in sliding_window(image, 1, (5, 5)):
      # Areas close to the center are usually occupied by confusing formations
      if (is_left_part and x >= max(contour_with_stats.cnt_last_non_zero_for_each_row[y], im_width / 3)) or \
          (not is_left_part and x <= min(contour_with_stats.cnt_first_non_zero_for_each_row[y], 2 * im_width / 3)):
        continue
      min_val, max_val, _, _ = cv.minMaxLoc(bb)
      avg_intensity_3ch, std_dev_intensity_3ch = cv.meanStdDev(bb)
      std_dev = std_dev_intensity_3ch[0]
      if max_val > self._params.border_line_min_val and \
          self._params.to_border_line_min_diff <= (max_val - min_val) <= self._params.to_border_line_max_diff and \
          min_val <= self._params.bb_with_border_line_min_val_thresh:
        mp_lung_points_only_image[y, x] = 255

    connectivity = self._params.components_connectivity

    connected_components = cv.connectedComponentsWithStats(mp_lung_points_only_image, connectivity, cv.CV_32S)
    return connected_components

  def filter_cc_points(self, evolving_image, centroid, cc_points, contour_with_stats, cc_components_image):
    def filter_parallel(point1, point2):
      _, _, points_in_line = get_line(point1, point2)
      non_last_cc_count = 0
      c_points_until_filtered = []

      for point in points_in_line:
          if cc_components_image[point[1], point[0]] == 0:
            non_last_cc_count += 1
          elif non_last_cc_count >= 1:
            return c_points_until_filtered
          else:
            c_points_until_filtered.append(point)
      return []
    image = evolving_image(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED).copy()
    contour = contour_with_stats.contour
    is_cnt_left_part = evolving_image.is_left_part

    filtered_points = []
    max_cnt_y = np.max(contour_with_stats.cnt_last_non_zero_for_each_column)
    for cc_point in cc_points:
      if (cc_point[0], cc_point[1]) in filtered_points:
        continue
      min_index, min_dist, closest_point = closest_contour_point(cc_point, contour)
      filtered_parallel = filter_parallel(cc_point, closest_point)
      if len(filtered_parallel) > 0:
        filtered_points.extend(filtered_parallel)
        continue
      if (is_cnt_left_part and cc_point[0] <= contour_with_stats.bounding_rect_top_left_x + \
          contour_with_stats.bounding_rect_width * 0.1) or \
          (not is_cnt_left_part and cc_point[0] >= contour_with_stats.bounding_rect_top_left_x + \
          contour_with_stats.bounding_rect_width * 0.9):
            continue
      # Filter out points below the contour

      y_last_non_zero = contour_with_stats.cnt_last_non_zero_for_each_column[cc_point[0]]
      y_last_non_zero = y_last_non_zero
      if y_last_non_zero == -1 or cc_point[1] <= y_last_non_zero:
        continue
      filtered_points.append((cc_point[0], cc_point[1]))

      continue
    return cc_points, filtered_points

  def keep_connected_component(self, image, centroid, labels, stats, label_idx, support_points_image, contour, support_points):
    im_height, im_width = image.shape
    min_index, min_dist, closest_point = closest_contour_point(centroid, contour)
    # When calculating line stats up to the connected components (external to contour) it is useful
    # that the intensity value at the closest point is pre-calculated as the average in an area
    # around this point but inside the contour. Because maybe this point is a border point, and its 
    # intensity value is misleading
    point_intensity = {closest_point: intensity_stats_around_point_in_contour(closest_point, contour, image)}
    cc_height = stats[label_idx][3]
    cc_width = stats[label_idx][2]
    cc_points = get_connected_component_points(labels, label_idx)
    cc_size = cc_points.shape[0]
    if cc_size < self._params.connected_component_min_size:
        return False, min_index
    if min_dist < self._params.border_to_contour_max_dist:
      centroid_point = (int(centroid[0]), int(centroid[1]))
      line_stats = intensity_stats_on_line(centroid_point, closest_point, image)
      support_point = (int((centroid_point[0] + closest_point[0]) / 2), int((centroid_point[1] + closest_point[1]) / 2))
      if line_stats is None:
        y_coord = support_point[1]
        x_coord = support_point[0]
        bb = image[max(0, y_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_height, y_coord + self._params.mp_points_intensity_calculation_bb_size),
                max(0, x_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_width, x_coord + self._params.mp_points_intensity_calculation_bb_size)]
        avg_intensity_3ch, std_dev_intensity_3ch = cv.meanStdDev(bb)
        avg_intensity = avg_intensity_3ch[0]
        max_diff = 0.
      else:
        avg_intensity = line_stats.mean
        max_diff = line_stats.max_diff
        if max_diff is None:
          # Do not allow single point components
          return False, min_index

      cv.circle(support_points_image, centroid_point, 5, (255, 255, 255))
      cv.circle(support_points_image, closest_point, 5, (0, 255, 255))
      support_points.append(support_point)
      cv.circle(support_points_image, (stats[label_idx, cv.CC_STAT_LEFT], stats[label_idx, cv.CC_STAT_TOP]), 3, (128, 128, 0))
      cv.circle(support_points_image, support_point, 5, (255, 0, 0))
      return True, min_index
    return False, min_index

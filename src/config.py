import json
from typing import NamedTuple


VISUALIZATION_PAPER_IMAGES = True
VISUALIZATION_GT = False
VISUALIZATION = False
VISUALIZATION_PREPROCESSING = False
VISUALIZATION_SEGMENTATOR = True
VISUALIZATION_REGION_GROWING_MPPOINTS_INTENSITY_BASED = False
VISUALIZATION_REGION_GROWING_CONVEX_HULL = True
VISUALIZATION_EVALUATOR = False
VISUALIZATION_FINAL_RESULT = True

class Params(NamedTuple):
    # Simple (threshold) segmentator
    intensity_lower_threshold: int = 5
    intensity_upper_threshold: int = 135
    intensity_probability_cutoff: float = 0.7

    # morphological transformer
    erosion_kernel_size: int = 1
    erosion_iterations: int = 1
    dilation_kernel_size: int = 1
    dilation_iterations: int = 1
    corners_erosion_kernel_size: int = 2
    corners_erosion_iterations: int = 1

    # CLAHE
    clip_limit: float = 0.8
    tile_grid_size: int = 8
    
    # Hough segmentator
    min_contour_area = 100. ** 2
    parallel_line_distance_variance_thresh: float = 40.
    min_parallel_lines: int = 2
    first_canny_threshold: int = 70
    second_canny_threshold: int = 160
    aperture_size: int = 3
    hough_min_line_length: int = 30
    hough_max_line_gap: int = 1
    hough_accumulator_threshold: int = 30

    # Combined hough segmentator
    mp_points_density_calculation_bb_size = 30
    mp_points_intensity_calculation_bb_size = 10
    harris_first_canny_threshold: int = 20
    harris_second_canny_threshold: int = 100
    harris_clahe_clip_limit: float = 2.0
    connected_component_min_size: int = 70
    max_size_contour_replacement: int = 200

    # Border Line Growable Hough Segmentator
    border_line_erosion_kernel_size: int = 2
    border_line_erosion_iterations: int = 1
    border_line_dilation_kernel_size: int = 3
    border_line_dilation_iterations: int = 1

    growable_segmentator_convex_hull_fill_max_distance_start_to_end: int = 50
    growable_segmentator_convex_hull_fill_max_distance_to_farthest: int = 50

    border_line_min_val: int = 210
    to_border_line_min_diff: int = 20
    to_border_line_max_diff: int = 150
    bb_with_border_line_min_val_thresh: int = 200

    components_connectivity: int = 3
    border_to_contour_max_dist: int = 40

    # Symmetry detector
    triangle_end_y: int = 100
    symmetry_line_column_shift: int = 0

    # Sternum extraction
    sternum_width: int = 30

    # Shoulder extraction
    shoulder_cut_line_width = 30
    histogram_bb_size: int = 14
    
    # Log transform
    log_transform_constant: float = 0.2

    # Region Growing
    convex_hull_unconstrained_fill_max_distance_start_to_end: int = 25
    convex_hull_mp_points_fill_max_distance_start_to_end: int = 150
    convex_hull_unconstrained_fill_max_distance_to_farthest: int = 25
    convex_hull_mp_points_fill_max_distance_to_farthest: int = 150
    convex_hull_mp_points_fill_min_density: int = 0.005
    convex_hull_actual_border_line_distance_start_to_end: int = 120
    convex_hull_actual_border_line_max_distance_to_farthest: int = 50
    separation_nearest_valley_points_threshold: int = 70
    convex_hull_separation_min_distance_to_farthest: int = 140
    convex_hull_separation_min_distance_farthest_to_line: int = 50
    convex_hull_valley_point_th = 10
    convex_hull_split_selection_density_ratio = 0.3

    # Flags
    run_region_growing = True
    growable_hough_run_convex_hull = True

    def save_to_file(self, directory_name):
      with open(f'{directory_name}/config.txt', 'w') as outfile:
          json.dump(self._asdict(), outfile)


default_params = Params()

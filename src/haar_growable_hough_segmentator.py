from collections import deque
import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

from src.growable_hough_segmentator import GrowableHoughSegmentator
from src.region_transformer import GrowProfile, RegionTransformer
from src.util import sliding_window, kmeans_clustering, get_connected_components_image, \
        closest_contour_point, get_connected_component_points, intensity_stats_on_line
from src.visualization_utils import EvolvingImage, visualize_two, visualize_images
from src.morphological_transformer import MorphologicalTransformer

class LocalGrowProfile(GrowProfile):
    def __init__(self, evolving_image, points):
        super().__init__(evolving_image, np.array(points))
        self._region_threshold = 20.
        self._pixel_intesity_difference_threshold = 20.
        self._to_region_mean_intensity_difference_threshold = 20.

class HaarGrowableHoughSegmentator(GrowableHoughSegmentator):
  def get_support_connected_components(self, evolving_image, filtered_segmented_image, contour_with_stats):
    image = evolving_image(EvolvingImage.Stage.HARRIS_CONTRAST_ENHANCED).copy()
    im_height, im_width = image.shape
    mp_lung_points_only_image =  np.zeros(image.shape, np.uint8)
    mp_lung_points_image = image.copy()
    corners_image = mp_lung_points_image.copy()
    corners_only_image = np.zeros(image.shape, np.uint8)
    edges = cv.Canny(image, self._params.harris_first_canny_threshold, self._params.harris_second_canny_threshold,
                     apertureSize = self._params.aperture_size)
    edges[:int(im_width / 3), :] = 0
    evolving_image.add_image_in_stage(EvolvingImage.Stage.HARRIS_EDGES, edges)
    dst = cv.cornerHarris(edges, 2, 3, 0.04)
    mp_lung_points_only_image[dst > 0.0 * dst.max()] = 128
    corners = np.where(mp_lung_points_only_image == 128)
    corner_points = []
    mp_lung_points = []
    for x_coord, y_coord in zip(corners[1], corners[0]):
        bb = image[max(0, y_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_height, y_coord + self._params.mp_points_intensity_calculation_bb_size),
                max(0, x_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_width, x_coord + self._params.mp_points_intensity_calculation_bb_size)]
        avg_intensity_3ch, std_dev_intensity_3ch = cv.meanStdDev(bb)
        avg_intensity = avg_intensity_3ch[0]
        std_dev_intensity = std_dev_intensity_3ch[0]

        if 20. <= abs(avg_intensity - self._params.intensity_upper_threshold) <= 80. and\
                std_dev_intensity < 50. and std_dev_intensity > 20.:
          corner_points.append([x_coord, y_coord, 3 * avg_intensity])
          mp_lung_points.append([x_coord, y_coord, avg_intensity])
          corners_only_image[y_coord, x_coord] = 255
          cv.circle(corners_image, (x_coord, y_coord), 2, (255, 255, 0))
    m_transformer = MorphologicalTransformer(self._params.corners_erosion_kernel_size, self._params.corners_erosion_iterations,
                                             self._params.dilation_kernel_size, self._params.dilation_iterations)
    tr_image = m_transformer.erode(corners_only_image)
    combined = np.maximum(np.maximum(image, tr_image), filtered_segmented_image)
    unique_corners = cv.bitwise_and(corners_only_image, corners_only_image, mask = cv.bitwise_not(filtered_segmented_image))
    connectivity = 3
    connected_components = cv.connectedComponentsWithStats(unique_corners, connectivity, cv.CV_32S)
    return connected_components

  def keep_connected_component(self, image, centroid, labels, stats, label_idx, support_points_image, contour, support_points):
    im_height, im_width = image.shape
    min_index, min_dist, closest_point = closest_contour_point(centroid, contour)
    CORNER_TO_CONTOUR_MAX_DIST = 40
    cc_height = stats[label_idx][3]
    cc_width = stats[label_idx][2]
    if min_dist < CORNER_TO_CONTOUR_MAX_DIST:
      centroid_point = (int(centroid[0]), int(centroid[1]))
      line_stats = intensity_stats_on_line(centroid_point, closest_point, image)
      support_point = (int((centroid_point[0] + closest_point[0]) / 2), int((centroid_point[1] + closest_point[1]) / 2))
      if line_stats is None:
        y_coord = support_point[1]
        x_coord = support_point[0]
        bb = image[max(0, y_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_height, y_coord + self._params.mp_points_intensity_calculation_bb_size),
                max(0, x_coord - self._params.mp_points_intensity_calculation_bb_size):
                min(im_width, x_coord + self._params.mp_points_intensity_calculation_bb_size)]
        avg_intensity_3ch, std_dev_intensity_3ch = cv.meanStdDev(bb)
        avg_intensity = avg_intensity_3ch[0]
        max_diff = 0.
      else:
        avg_intensity = line_stats.mean
        max_diff = line_stats.max_diff
        if max_diff is None:
          # Do not allow single point components
          return False, min_index

      cv.circle(support_points_image, centroid_point, 5, (255, 255, 255))
      cv.circle(support_points_image, closest_point, 5, (0, 255, 255))
      if abs(avg_intensity - self._params.intensity_upper_threshold) <= 80. and max_diff < 50.:
        support_points.append(support_point)
        cv.circle(support_points_image, (stats[label_idx, cv.CC_STAT_LEFT], stats[label_idx, cv.CC_STAT_TOP]), 3, (128, 128, 0))
        cv.circle(support_points_image, support_point, 5, (255, 0, 0))
        return True, min_index
    return False, min_index
